#include "DataPackage.h"

DataPackage::DataPackage(QString csv_path, QMap<QString, QList<float>> meassurements, int timestamp, int frequency):
	csv_path_(csv_path), meassurements_(meassurements), timestamp_(timestamp), frequency_(frequency)
{
}

QString DataPackage::get_path_to_csv_file()
{
	return csv_path_;
}

QList<float> DataPackage::get_values_for_channel(QString & channel)
{
	return meassurements_[channel];
}

int DataPackage::get_frequency()
{
	return frequency_;
}

int DataPackage::get_timestamp()
{
	return timestamp_;
}
