#pragma once

#include <qstringlist.h>
#include <qmap.h>

/**
* @brief The FileInput class provides functioons to read text from a file
*/
class FileInput
{
public:
	/**
	* @brief loadLines load all lines of a file
	* @param filepath relative or absolute path of the file to load
	* @return all lines of a file
	*/
	static QStringList loadLines(QString filepath);

	/**
	* @brief loadAsMap loads content of a file as a map. <br>
	*          e.g.: a::b <br>
	*          => seperator = "::"; key = "a", value = "b" <br>
	*          loadAsMap will skip lines which does not contain a seperator, key as well as value will be trimmed, therefore preceding as well as trailing whitespaces will be removed
	* @param filepath file too load
	* @param seperator whichever seperator is used, also whitespace is possible (" ")
	* @return key-value map
	*/
	static QMap<QString, QString> loadAsMap(QString filepath, QString seperator);

	/**
	* @brief listFiles list all files of a directory
	* @param path directoy path
	* @return list of files in the directory
	*/
	static QStringList listFiles(QString path);
};