#include "DBController.h"
#include "DataPackage.h"

DBController::DBController(QString&path): path_(path)
{
	//db_pointer_ = new DBManager(path_);
	// init the databse pointer
	db_pointer_ = std::make_unique<DBManager>(DBManager(path_));
} 

QList<QString> DBController::get_weapon_types()
{
	// query all entries of the weapon name table
	QString sql_query ("SELECT name FROM weapon_type ORDER BY name");
	QList<QString> weapon_list;

	if (db_pointer_->execute_sql(sql_query, result_) && !result_.isEmpty()) {
		//push every weapontype in the list
		for (DataRow data_row : result_) {
			weapon_list.append(data_row.get(QString("name")));
		} 
	}
	return weapon_list;
}

int DBController::get_weapon_type_id_by_name(QString & weapon_name)
{
	// query for selecting the entry of weaponame table via the id of a weapon
	QString sql_query("SELECT id FROM weapon_type WHERE name = '" + 
		weapon_name +"';");

	db_pointer_->execute_sql(sql_query, result_);

	if (result_.size()) {
		return result_[0].get(QString("id")).toInt();
	}
	else {
		return -1;
	}
}

void DBController::add_weapon_type(QString & weapon_name)
{
	// check if weeapon_name is not already present == (-1)
	if (get_weapon_type_id_by_name(weapon_name) < 0) {
		insert_params params;
		params["name"] = weapon_name;
		// insert into weapon_type table
		insert(QString("weapon_type"), params);
	}
}

QList<DataRow> DBController::get_results(int shooter_id)
{
	// query all results of a shooter
	QString sql_query("SELECT * FROM results WHERE shooter_id = " + 
		QString::number(shooter_id) + ";");

	//return results if possible, empty list otherwise
	if (db_pointer_->execute_sql(sql_query, result_)) {
		return result_;
	}
	else {
		return QList<DataRow>();
	}
}

QList<DataRow> DBController::get_results(QString & first_name, QString & last_name)
{
	return get_results(get_index_of_shooter(last_name, first_name));
}

QList<QString> DBController::get_disciplines()
{
	QList<QString> discipline_list;
	// query for all disciplines
	QString sql_query("SELECT name FROM discipline JOIN weapon_type ON weapon_type.id = discipline.weapon_type ORDER BY name;");
	
	db_pointer_->execute_sql(sql_query, result_);
	for (DataRow& data_row : result_) {
		discipline_list.append(data_row.get(QString("name")));
	}

	return discipline_list;
}

int DBController::get_discipline_id(QString & weapon)
{
	QString sql_query("SELECT d.id FROM discipline d JOIN weapon_type w ON d.weapon_type = w.id WHERE name '"+
		weapon + "' ;");

	if (db_pointer_->execute_sql(sql_query, result_)) {
		return result_[0].get(QString("id")).toInt();
	}
	else {
		return -1;
	}
}

int DBController::get_index_of_shooter(QString & last_name, QString & first_name)
{
	QString sql_query("SELECT id FROM shooter where name LIKE '" + last_name
	 + "' AND first_name LIKE '" + first_name + "';");

	if (!db_pointer_->execute_sql(sql_query, result_) || result_.isEmpty()) {
		return -1;
	}

	return result_.first().get(QString("id")).toInt();

}

int DBController::get_index_of_shooter(QString & full_name)
{
	QString sql_query("SELECT id, name, first_name FROM shooter;");

	if (!db_pointer_->execute_sql(sql_query, result_) || result_.isEmpty()) {
		return -1;
	}

	for (DataRow& data_row : result_) {
		QString complete_name = data_row.get(QString("first_name")) + " " + data_row.get(QString("name"));

		if ((data_row.get(QString("first_name")) + " " + data_row.get(QString("name")))  == full_name) {
			return data_row.get(QString("id")).toInt();
		}
	}

	return -1;
}

QString DBController::get_shooter(int index)
{
	QString sql_query("SELECT name, first_name FROM shooter WHERE id =" + 
		QString::number(index) + ";");

	if (db_pointer_->execute_sql(sql_query, result_) && !result_.isEmpty()) {
		return result_.first().get(QString("first_name")) + " " + result_.first().get(QString("name"));
	}

	return QString("");
}

QList<QString> DBController::get_shooter_names()
{
	QList<QString> names;

	for (const QPair<QString, QString>& name_pair : get_shooter_names_as_pair()) {
		names.append(name_pair.first + name_pair.second);
	}

	return names;
}

QList<QPair<QString, QString>> DBController::get_shooter_names_as_pair()
{
	QString sql_query("SELECT name, first_name FROM shooter ORDER BY first_name, name");

	db_pointer_->execute_sql(sql_query, result_);

	QList<QPair<QString, QString>> names_pair;

	for (DataRow& data_row : result_) {
		names_pair.append(QPair<QString, QString>(data_row.get(QString("first_name")), data_row.get(QString("name"))));
	}

	return names_pair;
}

void DBController::add_shooter(QString & first_name, QString & last_name, QString & laterality, QString & gender, QString & association, QDate & birth_date)
{
	QString lat = 0;
	QString gen = 0;

	QString sql_query("SELECT id FROM laterality WHERE name ='" + laterality + "';");
	
	bool ret = db_pointer_->execute_sql(sql_query, result_);
	
	if (!result_.isEmpty() && ret) {
		lat = result_.first().get(QString("id"));
	}

	
	sql_query = "SELECT id FROM gender WHERE name = '" + gender + "';";
	ret  = db_pointer_->execute_sql(sql_query, result_);
	if (!result_.isEmpty() && ret) {
		gen = result_.first().get(QString("id"));
	}
	
	insert_params params;
	params["name"] = last_name;
	params["first_name"] = first_name;
	params["gender_id"] = gen;
	params["lat_id"] = lat;
	params["association"] = association;
	params["birthdate"] = birth_date.toString("yyyy.MM.dd");

	insert(QString("shooter"), params);
	params.clear();

}

QList<QString> DBController::get_genders()
{
	QString sql_query("SELECT name FROM gender ;");
	db_pointer_->execute_sql(sql_query, result_);

	QList<QString> genders_list;

	for (DataRow& date_row : result_) {
		genders_list.append(date_row.get(QString("name")));
	}

	return genders_list;
}

QList<QString> DBController::get_lateralities()
{
	QString sql_query("SELECT name FROM laterality;");

	db_pointer_->execute_sql(sql_query, result_);
	QList<QString> laterality_list;

	for (DataRow& data_row : result_) {
		laterality_list.append(data_row.get(QString("name")));
	}

	return laterality_list;
}

int DBController::last_id_of(QString & table_name)
{
	QString sql_query("SELECT MAX(id) as id FROM " + table_name + ";");

	if (db_pointer_->execute_sql(sql_query, result_) && result_.size()) {
		return result_.last().get(QString("id")).toInt();
	}
	else {
		return -1;
	}
}

QList<QList<DataPackage>> DBController::get_data_of_result(int result_id)
{
	QList<QList<DataPackage>> data;
	QString sql_query("SELECT * FROM values WHERE result_id = " + 
				QString::number(result_id) + " ORDER BY meassurement_nr, timestamp;");

	if (db_pointer_->execute_sql(sql_query, result_)) {
		for (DataRow& data_row : result_) {
			if (data_row.get(QString("meassurement_nr")).toInt() > data.size()) {
				data.append(QList<DataPackage>());
			}
		}
	}
	//hier weitermachen sobald tabellen fertig

	return QList<QList<DataPackage>>();
}

bool DBController::insert(QString & table, const insert_params & parameters)
{
	QString keys("");
	QString values("");

	for ( QString& key : parameters.keys()) {
		
		keys += key + ", ";
		values += "'" + parameters[key] + "', ";
	}

	keys.chop(2);// letztes ", " entfernen
	values.chop(2);// letztes ", " entfernen

	QString sql_query("INSERT INTO " + table + "(" + keys + ") VALUES ("+ 
		values + ");");

	bool ret = db_pointer_->execute_sql(sql_query, result_);
	return ret;
}
