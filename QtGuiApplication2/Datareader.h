#pragma once

#include <qstring.h>
#include <qlist.h>
#include <qpair.h>
#include <qtextstream.h>
#include <qfile.h>
#include <qmap.h>



/**
* @brief The data_reader class reads from files and processes the input
*/
class data_reader {
public:
	/**
	* @brief Standard constructor
	*/
	data_reader();

	/**
	* @brief load_data_from_file loads an existent file and splits it according to a given delimiter
	* @param path path to the file
	* @param delim delimiter according to which to lines are split
	* @return list of of separated float values
	*/
	QList<float> load_data_from_file(const QString& path,  const char delim)const;

	/**
	* @brief split_line_into_values splits a line of a file due to a given delimiter
	* @param line line to split
	* @param delim delimiter to split the line to
	* @return list of splitted strings
	*/
	QList<QString> split_line_into_values(QString& line, const char* delim)const;

	/**
	* @brief create_time_meassurement_pairs creates a list of pairs of values an their meassurement time
	* @param meassurements list of meassurements
	* @param frequency frequency of the meassurement
	* @return list of meassurement value meassurement time pairs
	*/
	QList<QPair<float, float>> create_time_meassurement_pairs(QList<float>& meassurements, unsigned int frequency)const;

	void send_out_data_to_widget(QList<float>& meassurements_list)const;

	void send_out_data_to_widget(QMap<QString, QList<float>>& meassurements_list)const;

	
signals:

	void send_meassurement_to_channel_one(float meassurement);

	void send_meassurement_to_channel_two(float meassurement);

	void send_meassurement_to_channel_three(float meassurement);

	void send_meassurement_to_channel_four(float meassurement);
	
	
private:
	/**
	* @brief convert_string_list_to_float_list converts a list of strings to a list of float values
	* @param string_value_list list of string values
`	* @return list of converted float values
	*/
	QList<float> convert_string_list_to_float_list(QList<QString>& string_value_list) const;
	



};

/**
* @brief The data_writer class writes data to an output file 
*/
class data_writer {
public:
	/**
	* @brief Standard constructor
	*/
	data_writer();

	/**
	* @brief Constructor
	* @param data_name path/name for the output file 
	*/
	data_writer(QString& data_name);

	/**
	* @brief set_data_name sets the name/path of the output file
	* @param data_name path/name of the output file
	*/
	void set_data_name(QString& data_name);

	/**
	* @brief get_data_name returns the path/name of the current output file
	* @return path/name of the current output file
	*/
	QString get_data_name() const;
	
	/**
	* @brief write_string_to_file writes a single string to the output file 
	* @param line string to write to the output file 
	* @return true if successfull, false otherwise 
	*/
	bool write_string_to_file(QString& line);

	/**
	* @brief write_list_to_file writes a list of strings to the output file
	* @param line list of strings to write to the output file
	* @return true if successfull, false otherwise
	*/
	bool write_list_to_file(QList<QString>& q_list);
	
	/**
	* @brief write_list_to_file writes a list of pairs to the output file; format <first value> <second value>
	* @param line list of pairs of strings to write to the output file
	* @return true if successfull, false otherwise
	*/
	bool write_list_of_pairs_to_file(QList<QPair<QString, QString>>& pair_q_list);

	/**
	* @brief write_meassurements_as_csv writes a list of values to a csv file
	* @param channel channel the meassurements belong to 
	* @param values meassurement values of the channel
	* @return true if successfull, false otherwise
	*/
	bool write_meassurements_as_csv(QString& channel, QList <float>& values);

	/**
	* @brief write_meassurements_as_csv writes a map containing channels as keys and their meassurements as lists to a csv file
	* @param file_name name/path of the output file 
	* @param meassurements QMap containg the meassurements sorted according to their channel
	* @return true if successfull, false otherwise
	*/
	bool write_meassurements_as_csv(QString& file_name, QMap<QString, QList<float>>& meassurements);

	/**
	* @brief write_meassurements_as_csv writes a list of values to a csv file
	* @param file_name name/path of the file the measssurements should be written to
	* @param channel channel the meassurements belong to
	* @param values meassurement values of the channel
	* @return true if successfull, false otherwise
	*/
	bool write_meassurements_as_csv(QString& file_name, QString& channel, QList <float>& values);

private: 
	
	QString data_name_;/**< name/path of the output file*/
};