#pragma once
#include <qstring.h>
#include <qmap.h>
#include <qlist.h>


/**
* @brief The DataPackage class provides an abstraction for a data set of meassurements for one ore more channel(s)
*/
class DataPackage {
public:

	/**
	* @brief constructor
	* @param csv_path path to the corresponding csv file
	* @param meassurements list of meassurements
	* @param timestamp time stamp of the meassurement
	* @param frequency frequency of the meassurement
	*/
	DataPackage(QString csv_path, QMap<QString, QList<float>> meassurements, int timestamp, int frequency);

	/**
	* @brief get_path_to_csv_file returns the path to the corrresponding csv file
	* @return path to the csv file
	*/
	QString get_path_to_csv_file();
	
	/**
	* @brief get_values_for_channel returns a list of meassurements for a given channel
	* @param channel name of the channel for which meassurements should be returned
	* @return list containing the meassurements belonging to the given channel
	*/
	QList<float> get_values_for_channel(QString& channel);

	/**
	* @brief get_frequency returns the frequency belonging to the package
	* @return frequency of the package
	*/
	int get_frequency();

	/**
	* @brief get_timestamp returns the time stamp of the package
	* @return time stamp
	*/
	int get_timestamp();

private:

	QString csv_path_;/**< path to the csv file */

	QMap<QString, QList<float>> meassurements_;/**< QMap containg the meassurements and the according channels; key is the name of the channek, meassurements as QList */

	int timestamp_;/**< time stamp */

	int frequency_;/**< frequency */
};