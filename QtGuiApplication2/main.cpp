
#include <QtWidgets/QApplication>
#include <QtConcurrent\qtconcurrentrun.h>
#include <qthread.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string>


#include <GL\glew.h>
#include <GL\freeglut.h>

#include "oldaapi.h"
#include "QtGuiApplication2.h"
#include "WindowsProcedure.h"
#include "Datareader.h"
#include "DBController.h"

LRESULT WINAPI WndProci(HWND hWnd, UINT msg, WPARAM hAD, LPARAM lParam)
{
	switch (msg)
	{
	default:
		return DefWindowProc(hWnd, msg, hAD, lParam);
	}
}

int main(int argc, char *argv[])
{

	QList<QPair<float, float>> time_value_pairs;
	data_reader reader;
	QList<float> c1 = reader.load_data_from_file("Kanal_1.txt", ';');
	QList<float> c2 = reader.load_data_from_file("Kanal_2.txt", ';');
	QList<float> c3 = reader.load_data_from_file("Kanal_3.txt", ';');
	QList<float> c4 = reader.load_data_from_file("Kanal_4.txt", ';');

	QMap<unsigned int, QList<float>> test_meassurements;

	test_meassurements[1] = c1;
	test_meassurements[2] = c2;
	test_meassurements[3] = c3;
	test_meassurements[4] = c4;

	//time_value_pairs = reader.create_time_meassurement_pairs(testw, 1000);
	data_writer writer(QString("debug_log.txt"));

	
	/*

	QString lat = "Linkshaendig";
	QString verband = "Norddeutscher Schuetzenbund";
	DBController db_controller(QString("test.db"));
	
	writer.write_string_to_file(QString("Disziplinen:\n"));
	writer.write_list_to_file(db_controller.get_disciplines());

	writer.write_string_to_file(QString("Geschlechter:\n"));
	writer.write_list_to_file(db_controller.get_genders());

	writer.write_string_to_file(QString("Haendigkeiten:\n"));
	writer.write_list_to_file(db_controller.get_lateralities());
	
	writer.write_string_to_file(QString("Schuetzen:\n"));
	writer.write_list_to_file(db_controller.get_shooter_names());

	writer.write_string_to_file(QString("Waffen:\n"));
	writer.write_list_to_file(db_controller.get_weapon_types());

	writer.write_string_to_file(QString("Namen Schuetzen Paar:\n"));
	writer.write_list_of_pairs_to_file(db_controller.get_shooter_names_as_pair());
	
	writer.write_string_to_file(QString("Schuetze Peters id:\n"));
	writer.write_string_to_file(QString::number(db_controller.get_index_of_shooter(QString("hans Peters"))));
	
	writer.write_string_to_file(QString("Schuetze Peters:\n"));
	writer.write_string_to_file(db_controller.get_shooter(1));

	writer.write_string_to_file(QString("Luftpistole:\n"));
	writer.write_string_to_file(QString::number(db_controller.get_weapon_type_id_by_name(QString("Luftpistole"))));
	
	writer.write_string_to_file(QString("Letzte ID Schuetzentabelle:\n"));
	writer.write_string_to_file(QString::number(db_controller.last_id_of(QString("shooter"))));
	
	QString gen = "Maennlich";
	//db_controller.add_shooter(QString("hans"), QString("Peters"), lat, gen, verband, QDate(1992, 7, 17));
	*/
	QApplication a(argc, argv);
	QtGuiApplication2 w;
	WNDCLASS wc;
	memset(&wc, 0, sizeof(wc));
	wc.lpfnWndProc = WndProc;
	LPCWSTR test = L"DtConsoleClass";
	wc.lpszClassName = test;
	//das hier auskommentieren f�r reale Messung
	w.set_test_data_active(test_meassurements);
	RegisterClass(&wc);

	HWND hWnd = CreateWindow(test,
		NULL,
		NULL,
		0, 0, 0, 0,
		NULL,
		NULL,
		NULL,
		NULL);
	//hier die Komentare entfernen
	//windows_procedure::get_instance().set_window_class(wc);
	//windows_procedure::get_instance().set_window(hWnd);
	//windows_procedure::get_instance().start_thread();
	w.show();
	return a.exec();
}

void start(windows_procedure& windows_proc) {
	
	windows_proc.start_procedure();
}











