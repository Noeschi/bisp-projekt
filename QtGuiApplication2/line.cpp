#include "line.h"

Line2D::Line2D(Vertex2D &start, Vertex2D &end, QColor &color):
    start_(start), end_(end), color_(color)
{

}

Line2D::Line2D()
{

}

Vertex2D Line2D::getStart() const
{
    return start_;
}

Vertex2D Line2D::getEnd() const
{
    return end_;
}

Line2D &Line2D::operator=(const Line2D &lineElement)
{
	// assign member for assignment 
    this->start_ = lineElement.start_;
    this->end_ = lineElement.end_;
    this->color_ = lineElement.color_;
    return *this;
}

QColor Line2D::getColor() const
{
    return color_;
}
