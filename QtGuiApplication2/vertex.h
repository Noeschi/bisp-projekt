#ifndef VERTEX_H
#define VERTEX_H


#include <QVector3D>
#include <QVector2D>
#include <QColor>

/**
* @brief Vertex is a class representing a 3D vertex
*/
class Vertex
{
public:

    /**
     * @brief standard constructor
     */
     Vertex();

    /**
     * @brief constructor
     * @param position 3D position of the vertex
     */
    explicit Vertex(const QVector3D &position);

    /**
     * @brief constructor
     * @param position 3D position of the vertex
     * @param color color of the vertex
     */
    Vertex (const QVector3D &position, const QVector3D &color);


    /*************************************** getter **********************************/

    /**
     * @brief returns position of the vertex
     * @return position_
     */
    const QVector3D& position() const;

    /**
     * @brief returns color of the vertex
     * @return color_
     */
    const QVector3D& color() const;
	
    /**
     * @brief returns the offset between the Vertex and its m_position_
     * @return offset
     */
    static int positionOffset();

    /**
     * @brief returns the offset between the Vertex and its m_color_
     * @return m_color_
     */
    static int colorOffset();

    /**
     * @brief returns the stride(sizeOf(Vertex))
     * @return stride(sizeOf(Vertex))
     */
    static int stride();

    /**
     * @brief returns the id of a vertex
     * @return certexId_
     */
    const int getId()const;

    /*************************************** setter **********************************/

    /**
     * @brief sets the position of the vertex
     * @param position position the vertex should be set to
     */
    void setPosition(const QVector3D& position);

    /**
     * @brief sets the color of a vertex
     * @param color color the vertex shoudl be set to
     */
    void setColor(const QVector3D& color);

    /**
     * @brief sets the vertex id
     * @param vertexId id the vertex id should be set to
     */
    void setVertexId(int vertexId);

    static const int PositionTupleSize = 3;
    static const int ColorTupleSize = 3;


private:
    QVector3D m_position_;/**< position of the vertex*/
    QVector3D m_color_;/**< color of the vertex*/
    int vertexId_;/**< id of the vertex vertex*/
};


// Constructors
inline Vertex::Vertex() {}
inline Vertex::Vertex(const QVector3D &position) : m_position_(position) {}
inline Vertex::Vertex(const QVector3D &position, const QVector3D &color) : m_position_(position), m_color_(color) {}

// Accessors / Mutators
inline const QVector3D& Vertex::position() const { return m_position_; }
inline const QVector3D &Vertex::color() const { return m_color_; }
void inline Vertex::setPosition(const QVector3D& position) { m_position_ = position; }
void inline Vertex::setColor(const QVector3D &color) { m_color_ = color; }
void inline Vertex::setVertexId(int vertexId){vertexId_ = vertexId;}
inline const int Vertex::getId()const{ return vertexId_;}
// OpenGL Helpers
inline int Vertex::positionOffset() { return offsetof(Vertex, m_position_); }
inline int Vertex::colorOffset() { return offsetof(Vertex, m_color_); }
inline int Vertex::stride() { return sizeof(Vertex); }


/**
* @brief Vertex2D class represents a 2D vertex
*/
class Vertex2D
{
public:

	/**
	* @brief standard constructor
	*/
    Vertex2D();

	/**
	* @brief constructor
	* @param position QVector2D representing the position of the vertex
	*/
    explicit Vertex2D(const QVector2D &position);

	/**
	* @brief constructor
	*/
    explicit Vertex2D (const QVector2D &position, const QVector3D &color);

	/**
	* @brief position returns the position of the vertex
	* @return 2D position of the vertex
	*/
    const QVector2D& position() const;

	/**
	* @brief color returns the color of the vertex
	* @return color of the vertex 
	*/
    const QVector3D& color() const;

	/**
	* @brief setPosition assigns a new position to the vertex
	* @param position position to be set
	*/
    void setPosition(const QVector2D& position);
    
	/**
	* @brief setColor sets the color the color of the vertex
	* @param color color of the vertex that should be set
	*/
	void setColor(const QVector3D& color);
    
	/**
	* @brief setVertexId sets the id of the vertex 
	* @param vertexId id of the vertex that should be set
	*/
	void setVertexId(int vertexId);

	/**
	* @brief getId returns the id of a vertex
	* @return id of the vertex
	*/
	const int getId()const;

private:
    QVector2D m_position_;/**< vector containing the 2D position of the vector >*/
    QVector3D m_color_;/**< color of the vertex >*/
    int vertexId_;/**< id of the vertex >*/
};

// Constructors
inline Vertex2D::Vertex2D() {}
inline Vertex2D::Vertex2D(const QVector2D &position) : m_position_(position) {}
inline Vertex2D::Vertex2D(const QVector2D &position, const QVector3D &color) : m_position_(position), m_color_(color) {}

// Accessors / Mutators
inline const QVector2D& Vertex2D::position() const { return m_position_; }
inline const QVector3D &Vertex2D::color() const { return m_color_; }
void inline Vertex2D::setPosition(const QVector2D& position) { m_position_ = position; }
void inline Vertex2D::setColor(const QVector3D &color) { m_color_ = color; }
void inline Vertex2D::setVertexId(int vertexId){vertexId_ = vertexId;}
inline const int Vertex2D::getId()const{ return vertexId_;}


#endif // VERTEX_H
