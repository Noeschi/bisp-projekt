#pragma once
#include <windows.h>
#include <utility> 
#include <qobject.h>
#include <qthread.h>
#include <qlist.h>
#include <qmap.h>

#include <qtimer.h>
#include "oldaapi.h"


// variable which are used in more than one function
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM hAD, LPARAM lParam);

/**
* @brief  The channel_sender class emits signals containing new meassured values
*/
class channel_sender : public QObject {
	Q_OBJECT

public:

	
	channel_sender() {};


	/*
	
		QThread * thread_;
	QTimer* timer_;

	QList<float> channel_one_list_;

	QList<float> channel_two_list_;

	QList<float> channel_three_list_;

	QList<float> channel_four_list_;

	QList<float>::iterator channel_one_iterator_;

	QList<float>::iterator channel_two_iterator_;

	QList<float>::iterator channel_three_iterator_;

	QList<float>::iterator channel_four_iterator_;

	bool test_data_;
	
	
	*/


	channel_sender& operator = (channel_sender& other) {
		std::swap(thread_, other.thread_);
		std::swap(timer_, other.timer_);
		std::swap(channel_one_list_, other.channel_one_list_);
		std::swap(channel_two_list_, other.channel_two_list_);
		std::swap(channel_three_list_, other.channel_three_list_);
		std::swap(channel_four_list_, other.channel_four_list_);
		std::swap(channel_one_iterator_, other.channel_one_iterator_);
		std::swap(channel_two_iterator_, other.channel_two_iterator_);
		std::swap(channel_three_iterator_, other.channel_three_iterator_);
		std::swap(channel_four_iterator_, other.channel_four_iterator_);
		std::swap(test_data_, other.test_data_);
		return *this;
	}

	channel_sender(bool test_data);

	void set_test_data_active();

	void set_test_data_to_send(unsigned int channel, QList<float> meassurement_list);

	void send_out_test_data();

public slots:

	/**
	* @brief safe_meassurements_to_file sets the safe file flag of windows_procedure to true
	*/
	void safe_meassurements_to_file();

	void send_out_test_meassurement_signal();

	


signals:
	/**
	* @brief meassured_value_changed_channel_one emits signal if a value for channel one was meassured
	* @param value value that was meassured
	*/
	void meassured_value_changed_channel_one(float value);

	/**
	* @brief meassured_value_changed_channel_two emits signal if a value for channel one was meassured
	* @param value value that was meassured
	*/
	void meassured_value_changed_channel_two(float value);
	
	/**
	* @brief meassured_value_changed_channel_three emits signal if a value for channel one was meassured
	* @param value value that was meassured
	*/
	void meassured_value_changed_channel_three(float value);
	
	/**
	* @brief meassured_value_changed_channel_four emits signal if a value for channel one was meassured
	* @param value value that was meassured
	*/
	void meassured_value_changed_channel_four(float value);
	
	/**
	* @brief update_time emits a signal for the eplapsed time since the meassurement started
	* @param miliseconds miliseconds that eplapsed since start of the meassurement
	*/
	void update_time(int miliseconds);
	
	/**
	* @brief update_number_meassurements signal used for the number of meassurements meassured
	* @param number_meassurements number of meassurements
	*/
	void update_number_meassurements(unsigned int number_meassurements);

private:
	QThread * thread_;
	QTimer* timer_;

	QList<float> channel_one_list_;

	QList<float> channel_two_list_;

	QList<float> channel_three_list_;

	QList<float> channel_four_list_;

	QList<float>::iterator channel_one_iterator_;

	QList<float>::iterator channel_two_iterator_;

	QList<float>::iterator channel_three_iterator_;

	QList<float>::iterator channel_four_iterator_;

	bool test_data_;

};

/**
* @brief
*/
class data_acquisition_thread : public QThread {
	Q_OBJECT
public:
	void run() override;

};

/**
* @brief windows_procedure class is used to run the meassurement loop for the A/D-Wandler. Since there is only A/D-Wandler it is designed as a singleton
*/
class windows_procedure : QObject {
	Q_OBJECT
public:
	/**
	* @brief deconstructor
	*/
	~windows_procedure();
	
	/**
	* @brief get_instance method returns the singleton instance of the windows_procedure instance
	*/
	static windows_procedure& get_instance();

	/**
	* @brief set_window method is used to set the handle of the windwow
	* @param hwnd window handle to be set
	*/
	void set_window(HWND& hwnd);



	/**
	* @brief get_window returns the current window handle
	* @return returns the current window handle
	*/
	HWND get_window();

	/**
	* @brief set_window_class sets the current window class
	* @param wndclass window class to be set
	*/
	void set_window_class(WNDCLASS& wndclass);
	
	/**
	* @brief get_window_class returns the current window class 
	* @return returns the current window class
	*/
	WNDCLASS get_window_class();
	
	/**
	* @brief start_thread starts a Qtconcurrent thread which communicates with the A/D-Wandler
	*/
	void start_thread();

	/**
	* @brief start_procedure starts the procedure for collecting information from the A/D-Wandler
	* @return exit/error code if something went wrong
	*/
	int start_procedure();

	/**
	* @brief emit_signal_to_channel emits a signal containing a meassurement from a certain channel
	* @param channel_value pair containing the channal number as key and the meassurement that belongs to it
	*/
	void emit_signal_to_channel(std::pair<unsigned int, float> channel_value);

	/**
	* @brief set_safe_files sets a flag so that meassurements will be safed to a file
	* @param safe flag, if true data will be saved, if false saing will be stopped
	*/
	void set_safe_files(bool safe);
	
	/**
	* @brief stop_running should stop the start_procedure thread is yete not working properly
	*/
	void stop_running();

	/**
	* @brief is_running returns a boolean value describing if the thread is still runnning
	*/
	bool is_running();

	/**
	* @brief enable_live_data sets a flag so that live data is recorded from the A/D-wandler
	*/
	void enable_live_data();

	/**
	* @brief disable_live_data sets a flag which marks that the recording of live data should be stopped.
	*/
	void disable_live_data();

	/**
	* @brief get_live_data_enabled returns true if live data is enabled, false otherwise
	*/
	bool get_live_data_enabled();

	/**
	* @brief test_functio should do something what I completely forgot
	* @return somethin something
	*/
	int test_function();


	channel_sender sender;/**< emits the signals concerning channels and their signals> */

	std::list<float> value_list_;/**< contains all values meassured >*/
	

	
	HDASS hAD;/**< handle to subsystem >*/
	HDEV hDev; /**<  >*/
	HWND hWnd_;/**< window handle >*/
	bool stop_running_ = false; /**< flag to stop loop >*/
private:

	/**
	* @brief private standard constructor to enforce singleton structure
	*/
	windows_procedure() {};


	/**
	* @brief private copy constructor to enforce singleton structure
	*/
	windows_procedure(windows_procedure const&);

	/**
	* @brief private assignment operator to avoid further instances
	*/
	void operator=(windows_procedure const&);


	WNDCLASS wc_;/**< window class for calling the procedure >*/


	bool safe_files_= false;/**< flag for saving data to file >*/
	
	data_acquisition_thread* data_acquisition_thread_;/**< data_aquisition_thread for collecting data from the A/D-Wandler >*/

	bool live_data_ = true;/**< flag indicating that live data is used >*/

	std::list<std::pair<float, float>> loaded_data_channel_one_;/**< meassurement list for channel one >*/
	std::list<std::pair<float, float>> loaded_data_channel_two_;/**< meassurement list for channel two >*/
	std::list<std::pair<float, float>> loaded_data_channel_three_;/**< meassurement list for channel three >*/
	std::list<std::pair<float, float>> loaded_data_channel_four_;/**< meassurement list for channel four >*/
	
};