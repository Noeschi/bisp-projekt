#pragma once

#include<qlist.h>
#include<qpair.h>
#include<qmap.h>




class meassurement_analyzer {
public:

	meassurement_analyzer();

	meassurement_analyzer(float threshold_start_signal, float threshold_pressure_signal, unsigned int frequence, unsigned int time_of_meassurement, unsigned int offset);

	~meassurement_analyzer();

	unsigned int get_start_index(QList<float>& meass_list);

	float get_max_pressure(QList<float>& press_values);

	float get_threshold_start_signal();

	float get_threshold_shot_signal();

	void set_threshold_start_signal(float threshold);

	void set_threshold_shot_signal(float threshold);

	void set_time_of_meassurement(unsigned int time);

	void set_frequence(unsigned int frequence);

	void set_offset(unsigned int offset);

	QMap<unsigned int, QList<float>> trimm_meassurement(QMap<unsigned int, QList<float>>& meassurement_map);

	


private:

	float threshold_start_signal_;

	float threshold_shot_signal_;

	unsigned int frequence_;

	unsigned int time_of_meassurement_;

	unsigned int offset_;



};
