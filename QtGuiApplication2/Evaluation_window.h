#pragma once


#include <QtWidgets/qwidget.h>

#include "ui_Widget.h"

class evaluation_window : public QWidget
{
	Q_OBJECT
public:
	explicit evaluation_window(QWidget *parent = 0);
	~evaluation_window();
private:
	Ui::Evaluation *ui;
};