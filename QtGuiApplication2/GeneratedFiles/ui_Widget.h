/********************************************************************************
** Form generated from reading UI file 'Widget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <GlWidget.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Evaluation
{
public:
    evaluation_widget *widget;

    void setupUi(QWidget *Evaluation)
    {
        if (Evaluation->objectName().isEmpty())
            Evaluation->setObjectName(QStringLiteral("Evaluation"));
        Evaluation->resize(641, 599);
        widget = new evaluation_widget(Evaluation);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(70, 30, 301, 301));

        retranslateUi(Evaluation);

        QMetaObject::connectSlotsByName(Evaluation);
    } // setupUi

    void retranslateUi(QWidget *Evaluation)
    {
        Evaluation->setWindowTitle(QApplication::translate("Evaluation", "Form", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Evaluation: public Ui_Evaluation {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
