/****************************************************************************
** Meta object code from reading C++ file 'QtGuiApplication2.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../QtGuiApplication2.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QtGuiApplication2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QtGuiApplication2_t {
    QByteArrayData data[27];
    char stringdata0[552];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtGuiApplication2_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtGuiApplication2_t qt_meta_stringdata_QtGuiApplication2 = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QtGuiApplication2"
QT_MOC_LITERAL(1, 18, 19), // "speichern_triggered"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 21), // "emit_load_channel_one"
QT_MOC_LITERAL(4, 61, 12), // "std::string&"
QT_MOC_LITERAL(5, 74, 4), // "path"
QT_MOC_LITERAL(6, 79, 21), // "emit_load_channel_two"
QT_MOC_LITERAL(7, 101, 23), // "emit_load_channel_three"
QT_MOC_LITERAL(8, 125, 22), // "emit_load_channel_four"
QT_MOC_LITERAL(9, 148, 5), // "hello"
QT_MOC_LITERAL(10, 154, 4), // "test"
QT_MOC_LITERAL(11, 159, 26), // "update_first_channel_value"
QT_MOC_LITERAL(12, 186, 5), // "value"
QT_MOC_LITERAL(13, 192, 27), // "update_second_channel_value"
QT_MOC_LITERAL(14, 220, 26), // "update_third_channel_value"
QT_MOC_LITERAL(15, 247, 27), // "update_fourth_channel_value"
QT_MOC_LITERAL(16, 275, 19), // "update_elapsed_time"
QT_MOC_LITERAL(17, 295, 12), // "elapsed_time"
QT_MOC_LITERAL(18, 308, 30), // "update_amount_of_meassurements"
QT_MOC_LITERAL(19, 339, 20), // "amount_meassurements"
QT_MOC_LITERAL(20, 360, 22), // "on_speichern_triggered"
QT_MOC_LITERAL(21, 383, 17), // "on_stop_triggered"
QT_MOC_LITERAL(22, 401, 29), // "on_load_channel_one_triggered"
QT_MOC_LITERAL(23, 431, 29), // "on_load_channel_two_triggered"
QT_MOC_LITERAL(24, 461, 31), // "on_load_channel_three_triggered"
QT_MOC_LITERAL(25, 493, 30), // "on_load_channel_four_triggered"
QT_MOC_LITERAL(26, 524, 27) // "evaluation_button_triggered"

    },
    "QtGuiApplication2\0speichern_triggered\0"
    "\0emit_load_channel_one\0std::string&\0"
    "path\0emit_load_channel_two\0"
    "emit_load_channel_three\0emit_load_channel_four\0"
    "hello\0test\0update_first_channel_value\0"
    "value\0update_second_channel_value\0"
    "update_third_channel_value\0"
    "update_fourth_channel_value\0"
    "update_elapsed_time\0elapsed_time\0"
    "update_amount_of_meassurements\0"
    "amount_meassurements\0on_speichern_triggered\0"
    "on_stop_triggered\0on_load_channel_one_triggered\0"
    "on_load_channel_two_triggered\0"
    "on_load_channel_three_triggered\0"
    "on_load_channel_four_triggered\0"
    "evaluation_button_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtGuiApplication2[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x06 /* Public */,
       3,    1,  110,    2, 0x06 /* Public */,
       6,    1,  113,    2, 0x06 /* Public */,
       7,    1,  116,    2, 0x06 /* Public */,
       8,    1,  119,    2, 0x06 /* Public */,
       9,    1,  122,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    1,  125,    2, 0x08 /* Private */,
      13,    1,  128,    2, 0x08 /* Private */,
      14,    1,  131,    2, 0x08 /* Private */,
      15,    1,  134,    2, 0x08 /* Private */,
      16,    1,  137,    2, 0x08 /* Private */,
      18,    1,  140,    2, 0x08 /* Private */,
      20,    0,  143,    2, 0x08 /* Private */,
      21,    0,  144,    2, 0x08 /* Private */,
      22,    0,  145,    2, 0x08 /* Private */,
      23,    0,  146,    2, 0x08 /* Private */,
      24,    0,  147,    2, 0x08 /* Private */,
      25,    0,  148,    2, 0x08 /* Private */,
      26,    0,  149,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::Float,   10,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void, QMetaType::UInt,   19,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QtGuiApplication2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QtGuiApplication2 *_t = static_cast<QtGuiApplication2 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->speichern_triggered(); break;
        case 1: _t->emit_load_channel_one((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 2: _t->emit_load_channel_two((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 3: _t->emit_load_channel_three((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 4: _t->emit_load_channel_four((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 5: _t->hello((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->update_first_channel_value((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: _t->update_second_channel_value((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: _t->update_third_channel_value((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->update_fourth_channel_value((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 10: _t->update_elapsed_time((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->update_amount_of_meassurements((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 12: _t->on_speichern_triggered(); break;
        case 13: _t->on_stop_triggered(); break;
        case 14: _t->on_load_channel_one_triggered(); break;
        case 15: _t->on_load_channel_two_triggered(); break;
        case 16: _t->on_load_channel_three_triggered(); break;
        case 17: _t->on_load_channel_four_triggered(); break;
        case 18: _t->evaluation_button_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QtGuiApplication2::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QtGuiApplication2::speichern_triggered)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QtGuiApplication2::*_t)(std::string & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QtGuiApplication2::emit_load_channel_one)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QtGuiApplication2::*_t)(std::string & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QtGuiApplication2::emit_load_channel_two)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QtGuiApplication2::*_t)(std::string & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QtGuiApplication2::emit_load_channel_three)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QtGuiApplication2::*_t)(std::string & );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QtGuiApplication2::emit_load_channel_four)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QtGuiApplication2::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QtGuiApplication2::hello)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject QtGuiApplication2::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_QtGuiApplication2.data,
      qt_meta_data_QtGuiApplication2,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QtGuiApplication2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtGuiApplication2::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QtGuiApplication2.stringdata0))
        return static_cast<void*>(const_cast< QtGuiApplication2*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int QtGuiApplication2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void QtGuiApplication2::speichern_triggered()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QtGuiApplication2::emit_load_channel_one(std::string & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QtGuiApplication2::emit_load_channel_two(std::string & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QtGuiApplication2::emit_load_channel_three(std::string & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QtGuiApplication2::emit_load_channel_four(std::string & _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QtGuiApplication2::hello(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
