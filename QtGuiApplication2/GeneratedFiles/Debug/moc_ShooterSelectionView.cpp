/****************************************************************************
** Meta object code from reading C++ file 'ShooterSelectionView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../shooter/ShooterSelectionView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ShooterSelectionView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ShooterSelectionView_t {
    QByteArrayData data[4];
    char stringdata0[54];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ShooterSelectionView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ShooterSelectionView_t qt_meta_stringdata_ShooterSelectionView = {
    {
QT_MOC_LITERAL(0, 0, 20), // "ShooterSelectionView"
QT_MOC_LITERAL(1, 21, 10), // "clickedAdd"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 20) // "clickedAddDiscipline"

    },
    "ShooterSelectionView\0clickedAdd\0\0"
    "clickedAddDiscipline"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ShooterSelectionView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ShooterSelectionView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ShooterSelectionView *_t = static_cast<ShooterSelectionView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->clickedAdd(); break;
        case 1: _t->clickedAddDiscipline(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ShooterSelectionView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ShooterSelectionView::clickedAdd)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ShooterSelectionView::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ShooterSelectionView::clickedAddDiscipline)) {
                *result = 1;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject ShooterSelectionView::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ShooterSelectionView.data,
      qt_meta_data_ShooterSelectionView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ShooterSelectionView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ShooterSelectionView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ShooterSelectionView.stringdata0))
        return static_cast<void*>(const_cast< ShooterSelectionView*>(this));
    return QWidget::qt_metacast(_clname);
}

int ShooterSelectionView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void ShooterSelectionView::clickedAdd()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void ShooterSelectionView::clickedAddDiscipline()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
