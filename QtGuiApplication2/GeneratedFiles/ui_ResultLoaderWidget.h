/********************************************************************************
** Form generated from reading UI file 'ResultLoaderWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESULTLOADERWIDGET_H
#define UI_RESULTLOADERWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ResultLoaderWidget
{
public:
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QTableWidget *shooterTable;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QTableWidget *testTable;

    void setupUi(QWidget *ResultLoaderWidget)
    {
        if (ResultLoaderWidget->objectName().isEmpty())
            ResultLoaderWidget->setObjectName(QStringLiteral("ResultLoaderWidget"));
        ResultLoaderWidget->resize(962, 386);
        horizontalLayout_2 = new QHBoxLayout(ResultLoaderWidget);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(ResultLoaderWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        shooterTable = new QTableWidget(ResultLoaderWidget);
        if (shooterTable->columnCount() < 2)
            shooterTable->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        shooterTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        shooterTable->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        shooterTable->setObjectName(QStringLiteral("shooterTable"));

        verticalLayout->addWidget(shooterTable);


        horizontalLayout_2->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, -1, 0, -1);
        label_2 = new QLabel(ResultLoaderWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        testTable = new QTableWidget(ResultLoaderWidget);
        if (testTable->columnCount() < 3)
            testTable->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        testTable->setHorizontalHeaderItem(0, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        testTable->setHorizontalHeaderItem(1, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        testTable->setHorizontalHeaderItem(2, __qtablewidgetitem4);
        testTable->setObjectName(QStringLiteral("testTable"));

        verticalLayout_2->addWidget(testTable);


        horizontalLayout_2->addLayout(verticalLayout_2);


        retranslateUi(ResultLoaderWidget);

        QMetaObject::connectSlotsByName(ResultLoaderWidget);
    } // setupUi

    void retranslateUi(QWidget *ResultLoaderWidget)
    {
        ResultLoaderWidget->setWindowTitle(QApplication::translate("ResultLoaderWidget", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("ResultLoaderWidget", "Sportler:", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem = shooterTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("ResultLoaderWidget", "Vorname", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = shooterTable->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("ResultLoaderWidget", "Nachname", Q_NULLPTR));
        label_2->setText(QApplication::translate("ResultLoaderWidget", "Tests:", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem2 = testTable->horizontalHeaderItem(0);
        ___qtablewidgetitem2->setText(QApplication::translate("ResultLoaderWidget", "Datum", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem3 = testTable->horizontalHeaderItem(1);
        ___qtablewidgetitem3->setText(QApplication::translate("ResultLoaderWidget", "Test", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem4 = testTable->horizontalHeaderItem(2);
        ___qtablewidgetitem4->setText(QApplication::translate("ResultLoaderWidget", "id", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ResultLoaderWidget: public Ui_ResultLoaderWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESULTLOADERWIDGET_H
