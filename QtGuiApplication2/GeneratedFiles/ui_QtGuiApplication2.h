/********************************************************************************
** Form generated from reading UI file 'QtGuiApplication2.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTGUIAPPLICATION2_H
#define UI_QTGUIAPPLICATION2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "GlWidget.h"

QT_BEGIN_NAMESPACE

class Ui_QtGuiApplication2Class
{
public:
    QAction *actionSpeichern;
    QAction *actionStoppen;
    QAction *actionKanal_1;
    QAction *actionKanal_2;
    QAction *actionKanal_3;
    QAction *actionKanal_4;
    QAction *actionPushed_evaluation;
    QWidget *centralWidget;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *channel_one;
    QLabel *channel_two;
    QLabel *channel_three;
    QLabel *channel_four;
    QLabel *label_9;
    QLabel *label_messungen;
    QLabel *label_11;
    QLabel *label_zeit;
    gl_widget *glwidget_one;
    gl_widget *glwidget_two;
    QLabel *label_5;
    QLabel *label_6;
    gl_widget *glwidget_three;
    gl_widget *glwidget_4;
    QLabel *label_7;
    QLabel *label_8;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *evaluation_button;
    QMenuBar *menuBar;
    QMenu *menuDatei;
    QMenu *menuLaden;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QtGuiApplication2Class)
    {
        if (QtGuiApplication2Class->objectName().isEmpty())
            QtGuiApplication2Class->setObjectName(QStringLiteral("QtGuiApplication2Class"));
        QtGuiApplication2Class->setWindowModality(Qt::ApplicationModal);
        QtGuiApplication2Class->resize(1083, 638);
        actionSpeichern = new QAction(QtGuiApplication2Class);
        actionSpeichern->setObjectName(QStringLiteral("actionSpeichern"));
        actionStoppen = new QAction(QtGuiApplication2Class);
        actionStoppen->setObjectName(QStringLiteral("actionStoppen"));
        actionKanal_1 = new QAction(QtGuiApplication2Class);
        actionKanal_1->setObjectName(QStringLiteral("actionKanal_1"));
        actionKanal_2 = new QAction(QtGuiApplication2Class);
        actionKanal_2->setObjectName(QStringLiteral("actionKanal_2"));
        actionKanal_3 = new QAction(QtGuiApplication2Class);
        actionKanal_3->setObjectName(QStringLiteral("actionKanal_3"));
        actionKanal_4 = new QAction(QtGuiApplication2Class);
        actionKanal_4->setObjectName(QStringLiteral("actionKanal_4"));
        actionPushed_evaluation = new QAction(QtGuiApplication2Class);
        actionPushed_evaluation->setObjectName(QStringLiteral("actionPushed_evaluation"));
        centralWidget = new QWidget(QtGuiApplication2Class);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        formLayoutWidget = new QWidget(centralWidget);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(700, 40, 201, 211));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        channel_one = new QLabel(formLayoutWidget);
        channel_one->setObjectName(QStringLiteral("channel_one"));

        formLayout->setWidget(0, QFormLayout::FieldRole, channel_one);

        channel_two = new QLabel(formLayoutWidget);
        channel_two->setObjectName(QStringLiteral("channel_two"));

        formLayout->setWidget(1, QFormLayout::FieldRole, channel_two);

        channel_three = new QLabel(formLayoutWidget);
        channel_three->setObjectName(QStringLiteral("channel_three"));

        formLayout->setWidget(2, QFormLayout::FieldRole, channel_three);

        channel_four = new QLabel(formLayoutWidget);
        channel_four->setObjectName(QStringLiteral("channel_four"));

        formLayout->setWidget(3, QFormLayout::FieldRole, channel_four);

        label_9 = new QLabel(formLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_9);

        label_messungen = new QLabel(formLayoutWidget);
        label_messungen->setObjectName(QStringLiteral("label_messungen"));

        formLayout->setWidget(4, QFormLayout::FieldRole, label_messungen);

        label_11 = new QLabel(formLayoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_11);

        label_zeit = new QLabel(formLayoutWidget);
        label_zeit->setObjectName(QStringLiteral("label_zeit"));

        formLayout->setWidget(5, QFormLayout::FieldRole, label_zeit);

        glwidget_one = new gl_widget(centralWidget);
        glwidget_one->setObjectName(QStringLiteral("glwidget_one"));
        glwidget_one->setGeometry(QRect(50, 60, 271, 191));
        glwidget_two = new gl_widget(centralWidget);
        glwidget_two->setObjectName(QStringLiteral("glwidget_two"));
        glwidget_two->setGeometry(QRect(50, 280, 271, 191));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(50, 40, 55, 16));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(50, 260, 55, 16));
        glwidget_three = new gl_widget(centralWidget);
        glwidget_three->setObjectName(QStringLiteral("glwidget_three"));
        glwidget_three->setGeometry(QRect(340, 60, 271, 191));
        glwidget_4 = new gl_widget(centralWidget);
        glwidget_4->setObjectName(QStringLiteral("glwidget_4"));
        glwidget_4->setGeometry(QRect(340, 280, 271, 191));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(340, 40, 55, 16));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(340, 260, 55, 16));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(650, 330, 93, 28));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(650, 360, 93, 28));
        evaluation_button = new QPushButton(centralWidget);
        evaluation_button->setObjectName(QStringLiteral("evaluation_button"));
        evaluation_button->setGeometry(QRect(650, 390, 93, 28));
        QtGuiApplication2Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QtGuiApplication2Class);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1083, 26));
        menuDatei = new QMenu(menuBar);
        menuDatei->setObjectName(QStringLiteral("menuDatei"));
        menuLaden = new QMenu(menuDatei);
        menuLaden->setObjectName(QStringLiteral("menuLaden"));
        QtGuiApplication2Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QtGuiApplication2Class);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        QtGuiApplication2Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(QtGuiApplication2Class);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        QtGuiApplication2Class->setStatusBar(statusBar);

        menuBar->addAction(menuDatei->menuAction());
        menuDatei->addAction(actionSpeichern);
        menuDatei->addAction(menuLaden->menuAction());
        menuLaden->addAction(actionKanal_1);
        menuLaden->addAction(actionKanal_2);
        menuLaden->addAction(actionKanal_3);
        menuLaden->addAction(actionKanal_4);

        retranslateUi(QtGuiApplication2Class);

        QMetaObject::connectSlotsByName(QtGuiApplication2Class);
    } // setupUi

    void retranslateUi(QMainWindow *QtGuiApplication2Class)
    {
        QtGuiApplication2Class->setWindowTitle(QApplication::translate("QtGuiApplication2Class", "QtGuiApplication2", Q_NULLPTR));
        actionSpeichern->setText(QApplication::translate("QtGuiApplication2Class", "Speichern", Q_NULLPTR));
        actionStoppen->setText(QApplication::translate("QtGuiApplication2Class", "Stoppen", Q_NULLPTR));
        actionKanal_1->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 1", Q_NULLPTR));
        actionKanal_2->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 2", Q_NULLPTR));
        actionKanal_3->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 3", Q_NULLPTR));
        actionKanal_4->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 4", Q_NULLPTR));
        actionPushed_evaluation->setText(QApplication::translate("QtGuiApplication2Class", "pushed_evaluation", Q_NULLPTR));
        label->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 1:", Q_NULLPTR));
        label_2->setText(QApplication::translate("QtGuiApplication2Class", "Kanel 2:", Q_NULLPTR));
        label_3->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 3:", Q_NULLPTR));
        label_4->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 4:", Q_NULLPTR));
        channel_one->setText(QApplication::translate("QtGuiApplication2Class", "TextLabel", Q_NULLPTR));
        channel_two->setText(QApplication::translate("QtGuiApplication2Class", "TextLabel", Q_NULLPTR));
        channel_three->setText(QApplication::translate("QtGuiApplication2Class", "TextLabel", Q_NULLPTR));
        channel_four->setText(QApplication::translate("QtGuiApplication2Class", "TextLabel", Q_NULLPTR));
        label_9->setText(QApplication::translate("QtGuiApplication2Class", "#Messungen:", Q_NULLPTR));
        label_messungen->setText(QApplication::translate("QtGuiApplication2Class", "TextLabel", Q_NULLPTR));
        label_11->setText(QApplication::translate("QtGuiApplication2Class", "Zeit:", Q_NULLPTR));
        label_zeit->setText(QApplication::translate("QtGuiApplication2Class", "TextLabel", Q_NULLPTR));
        label_5->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 1", Q_NULLPTR));
        label_6->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 2", Q_NULLPTR));
        label_7->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 3", Q_NULLPTR));
        label_8->setText(QApplication::translate("QtGuiApplication2Class", "Kanal 4", Q_NULLPTR));
        pushButton->setText(QApplication::translate("QtGuiApplication2Class", "Abspielen", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("QtGuiApplication2Class", "Stoppen", Q_NULLPTR));
        evaluation_button->setText(QApplication::translate("QtGuiApplication2Class", "Evaluation", Q_NULLPTR));
        menuDatei->setTitle(QApplication::translate("QtGuiApplication2Class", "Datei", Q_NULLPTR));
        menuLaden->setTitle(QApplication::translate("QtGuiApplication2Class", "Laden", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class QtGuiApplication2Class: public Ui_QtGuiApplication2Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTGUIAPPLICATION2_H
