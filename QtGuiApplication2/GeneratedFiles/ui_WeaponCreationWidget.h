/********************************************************************************
** Form generated from reading UI file 'WeaponCreationWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WEAPONCREATIONWIDGET_H
#define UI_WEAPONCREATIONWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WeaponCreationWidget
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *weaponType;
    QDoubleSpinBox *gauge;

    void setupUi(QWidget *WeaponCreationWidget)
    {
        if (WeaponCreationWidget->objectName().isEmpty())
            WeaponCreationWidget->setObjectName(QStringLiteral("WeaponCreationWidget"));
        WeaponCreationWidget->resize(400, 102);
        formLayout = new QFormLayout(WeaponCreationWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(WeaponCreationWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        label_2 = new QLabel(WeaponCreationWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        weaponType = new QLineEdit(WeaponCreationWidget);
        weaponType->setObjectName(QStringLiteral("weaponType"));

        formLayout->setWidget(1, QFormLayout::FieldRole, weaponType);

        gauge = new QDoubleSpinBox(WeaponCreationWidget);
        gauge->setObjectName(QStringLiteral("gauge"));

        formLayout->setWidget(2, QFormLayout::FieldRole, gauge);


        retranslateUi(WeaponCreationWidget);

        QMetaObject::connectSlotsByName(WeaponCreationWidget);
    } // setupUi

    void retranslateUi(QWidget *WeaponCreationWidget)
    {
        WeaponCreationWidget->setWindowTitle(QApplication::translate("WeaponCreationWidget", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("WeaponCreationWidget", "Sportger\303\244t:", Q_NULLPTR));
        label_2->setText(QApplication::translate("WeaponCreationWidget", "Kaliber in mm:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class WeaponCreationWidget: public Ui_WeaponCreationWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WEAPONCREATIONWIDGET_H
