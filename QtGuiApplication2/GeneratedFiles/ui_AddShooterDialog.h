/********************************************************************************
** Form generated from reading UI file 'AddShooterDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDSHOOTERDIALOG_H
#define UI_ADDSHOOTERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddShooterDialog
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_5;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_6;
    QLineEdit *first_name;
    QLineEdit *name;
    QDateEdit *birthdate;
    QComboBox *gender;
    QComboBox *laterality;
    QComboBox *associationBox;

    void setupUi(QWidget *AddShooterDialog)
    {
        if (AddShooterDialog->objectName().isEmpty())
            AddShooterDialog->setObjectName(QStringLiteral("AddShooterDialog"));
        AddShooterDialog->resize(400, 300);
        formLayout = new QFormLayout(AddShooterDialog);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(AddShooterDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(AddShooterDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_5 = new QLabel(AddShooterDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_5);

        label_3 = new QLabel(AddShooterDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(AddShooterDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_4);

        label_6 = new QLabel(AddShooterDialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        first_name = new QLineEdit(AddShooterDialog);
        first_name->setObjectName(QStringLiteral("first_name"));

        formLayout->setWidget(0, QFormLayout::FieldRole, first_name);

        name = new QLineEdit(AddShooterDialog);
        name->setObjectName(QStringLiteral("name"));

        formLayout->setWidget(1, QFormLayout::FieldRole, name);

        birthdate = new QDateEdit(AddShooterDialog);
        birthdate->setObjectName(QStringLiteral("birthdate"));

        formLayout->setWidget(2, QFormLayout::FieldRole, birthdate);

        gender = new QComboBox(AddShooterDialog);
        gender->setObjectName(QStringLiteral("gender"));

        formLayout->setWidget(3, QFormLayout::FieldRole, gender);

        laterality = new QComboBox(AddShooterDialog);
        laterality->setObjectName(QStringLiteral("laterality"));

        formLayout->setWidget(4, QFormLayout::FieldRole, laterality);

        associationBox = new QComboBox(AddShooterDialog);
        associationBox->setObjectName(QStringLiteral("associationBox"));

        formLayout->setWidget(5, QFormLayout::FieldRole, associationBox);


        retranslateUi(AddShooterDialog);

        QMetaObject::connectSlotsByName(AddShooterDialog);
    } // setupUi

    void retranslateUi(QWidget *AddShooterDialog)
    {
        AddShooterDialog->setWindowTitle(QApplication::translate("AddShooterDialog", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("AddShooterDialog", "Vorname:", Q_NULLPTR));
        label_2->setText(QApplication::translate("AddShooterDialog", "Nachname:", Q_NULLPTR));
        label_5->setText(QApplication::translate("AddShooterDialog", "Geburtsdatum:", Q_NULLPTR));
        label_3->setText(QApplication::translate("AddShooterDialog", "Geschlecht:", Q_NULLPTR));
        label_4->setText(QApplication::translate("AddShooterDialog", "Seitigkeit:", Q_NULLPTR));
        label_6->setText(QApplication::translate("AddShooterDialog", "Verband:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AddShooterDialog: public Ui_AddShooterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDSHOOTERDIALOG_H
