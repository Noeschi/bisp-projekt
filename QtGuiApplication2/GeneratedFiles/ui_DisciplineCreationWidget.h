/********************************************************************************
** Form generated from reading UI file 'DisciplineCreationWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DISCIPLINECREATIONWIDGET_H
#define UI_DISCIPLINECREATIONWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DisciplineCreationWidget
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QComboBox *weapon_type;
    QPushButton *addWeapon;
    QLabel *label_2;
    QDoubleSpinBox *distance;
    QLabel *label_3;
    QComboBox *targetChooser;

    void setupUi(QWidget *DisciplineCreationWidget)
    {
        if (DisciplineCreationWidget->objectName().isEmpty())
            DisciplineCreationWidget->setObjectName(QStringLiteral("DisciplineCreationWidget"));
        DisciplineCreationWidget->resize(400, 300);
        verticalLayout = new QVBoxLayout(DisciplineCreationWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(DisciplineCreationWidget);
        label->setObjectName(QStringLiteral("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(label);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        weapon_type = new QComboBox(DisciplineCreationWidget);
        weapon_type->setObjectName(QStringLiteral("weapon_type"));

        horizontalLayout->addWidget(weapon_type);

        addWeapon = new QPushButton(DisciplineCreationWidget);
        addWeapon->setObjectName(QStringLiteral("addWeapon"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(addWeapon->sizePolicy().hasHeightForWidth());
        addWeapon->setSizePolicy(sizePolicy1);
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        addWeapon->setFont(font);

        horizontalLayout->addWidget(addWeapon);


        verticalLayout->addLayout(horizontalLayout);

        label_2 = new QLabel(DisciplineCreationWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(label_2);

        distance = new QDoubleSpinBox(DisciplineCreationWidget);
        distance->setObjectName(QStringLiteral("distance"));
        distance->setMaximum(5000);

        verticalLayout->addWidget(distance);

        label_3 = new QLabel(DisciplineCreationWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(label_3);

        targetChooser = new QComboBox(DisciplineCreationWidget);
        targetChooser->setObjectName(QStringLiteral("targetChooser"));

        verticalLayout->addWidget(targetChooser);


        retranslateUi(DisciplineCreationWidget);

        QMetaObject::connectSlotsByName(DisciplineCreationWidget);
    } // setupUi

    void retranslateUi(QWidget *DisciplineCreationWidget)
    {
        DisciplineCreationWidget->setWindowTitle(QApplication::translate("DisciplineCreationWidget", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("DisciplineCreationWidget", "Sportger\303\244t:", Q_NULLPTR));
        addWeapon->setText(QApplication::translate("DisciplineCreationWidget", "+", Q_NULLPTR));
        label_2->setText(QApplication::translate("DisciplineCreationWidget", "Distanz in Meter:", Q_NULLPTR));
        label_3->setText(QApplication::translate("DisciplineCreationWidget", "Zielscheibe:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DisciplineCreationWidget: public Ui_DisciplineCreationWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DISCIPLINECREATIONWIDGET_H
