/****************************************************************************
** Meta object code from reading C++ file 'WindowsProcedure.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../WindowsProcedure.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'WindowsProcedure.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_channel_sender_t {
    QByteArrayData data[12];
    char stringdata0[269];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_channel_sender_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_channel_sender_t qt_meta_stringdata_channel_sender = {
    {
QT_MOC_LITERAL(0, 0, 14), // "channel_sender"
QT_MOC_LITERAL(1, 15, 35), // "meassured_value_changed_chann..."
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 5), // "value"
QT_MOC_LITERAL(4, 58, 35), // "meassured_value_changed_chann..."
QT_MOC_LITERAL(5, 94, 37), // "meassured_value_changed_chann..."
QT_MOC_LITERAL(6, 132, 36), // "meassured_value_changed_chann..."
QT_MOC_LITERAL(7, 169, 11), // "update_time"
QT_MOC_LITERAL(8, 181, 11), // "miliseconds"
QT_MOC_LITERAL(9, 193, 27), // "update_number_meassurements"
QT_MOC_LITERAL(10, 221, 20), // "number_meassurements"
QT_MOC_LITERAL(11, 242, 26) // "safe_meassurements_to_file"

    },
    "channel_sender\0meassured_value_changed_channel_one\0"
    "\0value\0meassured_value_changed_channel_two\0"
    "meassured_value_changed_channel_three\0"
    "meassured_value_changed_channel_four\0"
    "update_time\0miliseconds\0"
    "update_number_meassurements\0"
    "number_meassurements\0safe_meassurements_to_file"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_channel_sender[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       4,    1,   52,    2, 0x06 /* Public */,
       5,    1,   55,    2, 0x06 /* Public */,
       6,    1,   58,    2, 0x06 /* Public */,
       7,    1,   61,    2, 0x06 /* Public */,
       9,    1,   64,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,   67,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::UInt,   10,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void channel_sender::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        channel_sender *_t = static_cast<channel_sender *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->meassured_value_changed_channel_one((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: _t->meassured_value_changed_channel_two((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->meassured_value_changed_channel_three((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: _t->meassured_value_changed_channel_four((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: _t->update_time((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->update_number_meassurements((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 6: _t->safe_meassurements_to_file(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (channel_sender::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&channel_sender::meassured_value_changed_channel_one)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (channel_sender::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&channel_sender::meassured_value_changed_channel_two)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (channel_sender::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&channel_sender::meassured_value_changed_channel_three)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (channel_sender::*_t)(float );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&channel_sender::meassured_value_changed_channel_four)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (channel_sender::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&channel_sender::update_time)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (channel_sender::*_t)(unsigned int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&channel_sender::update_number_meassurements)) {
                *result = 5;
                return;
            }
        }
    }
}

const QMetaObject channel_sender::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_channel_sender.data,
      qt_meta_data_channel_sender,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *channel_sender::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *channel_sender::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_channel_sender.stringdata0))
        return static_cast<void*>(const_cast< channel_sender*>(this));
    return QObject::qt_metacast(_clname);
}

int channel_sender::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void channel_sender::meassured_value_changed_channel_one(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void channel_sender::meassured_value_changed_channel_two(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void channel_sender::meassured_value_changed_channel_three(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void channel_sender::meassured_value_changed_channel_four(float _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void channel_sender::update_time(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void channel_sender::update_number_meassurements(unsigned int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
struct qt_meta_stringdata_data_acquisition_thread_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_data_acquisition_thread_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_data_acquisition_thread_t qt_meta_stringdata_data_acquisition_thread = {
    {
QT_MOC_LITERAL(0, 0, 23) // "data_acquisition_thread"

    },
    "data_acquisition_thread"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_data_acquisition_thread[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void data_acquisition_thread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject data_acquisition_thread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_data_acquisition_thread.data,
      qt_meta_data_data_acquisition_thread,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *data_acquisition_thread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *data_acquisition_thread::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_data_acquisition_thread.stringdata0))
        return static_cast<void*>(const_cast< data_acquisition_thread*>(this));
    return QThread::qt_metacast(_clname);
}

int data_acquisition_thread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_windows_procedure_t {
    QByteArrayData data[1];
    char stringdata0[18];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_windows_procedure_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_windows_procedure_t qt_meta_stringdata_windows_procedure = {
    {
QT_MOC_LITERAL(0, 0, 17) // "windows_procedure"

    },
    "windows_procedure"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_windows_procedure[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void windows_procedure::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject windows_procedure::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_windows_procedure.data,
      qt_meta_data_windows_procedure,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *windows_procedure::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *windows_procedure::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_windows_procedure.stringdata0))
        return static_cast<void*>(const_cast< windows_procedure*>(this));
    return QObject::qt_metacast(_clname);
}

int windows_procedure::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
