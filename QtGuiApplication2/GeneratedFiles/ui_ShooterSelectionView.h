/********************************************************************************
** Form generated from reading UI file 'ShooterSelectionView.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOOTERSELECTIONVIEW_H
#define UI_SHOOTERSELECTIONVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ShooterSelectionView
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QComboBox *shooterName;
    QLabel *label_2;
    QPushButton *addShooter;
    QLabel *label_3;
    QComboBox *discipline;
    QLabel *label_4;
    QPushButton *addDiszipline;

    void setupUi(QWidget *ShooterSelectionView)
    {
        if (ShooterSelectionView->objectName().isEmpty())
            ShooterSelectionView->setObjectName(QStringLiteral("ShooterSelectionView"));
        ShooterSelectionView->resize(400, 300);
        formLayout = new QFormLayout(ShooterSelectionView);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(ShooterSelectionView);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        shooterName = new QComboBox(ShooterSelectionView);
        shooterName->setObjectName(QStringLiteral("shooterName"));

        formLayout->setWidget(0, QFormLayout::FieldRole, shooterName);

        label_2 = new QLabel(ShooterSelectionView);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        addShooter = new QPushButton(ShooterSelectionView);
        addShooter->setObjectName(QStringLiteral("addShooter"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(addShooter->sizePolicy().hasHeightForWidth());
        addShooter->setSizePolicy(sizePolicy);
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        addShooter->setFont(font);

        formLayout->setWidget(1, QFormLayout::FieldRole, addShooter);

        label_3 = new QLabel(ShooterSelectionView);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        discipline = new QComboBox(ShooterSelectionView);
        discipline->setObjectName(QStringLiteral("discipline"));

        formLayout->setWidget(2, QFormLayout::FieldRole, discipline);

        label_4 = new QLabel(ShooterSelectionView);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        addDiszipline = new QPushButton(ShooterSelectionView);
        addDiszipline->setObjectName(QStringLiteral("addDiszipline"));
        sizePolicy.setHeightForWidth(addDiszipline->sizePolicy().hasHeightForWidth());
        addDiszipline->setSizePolicy(sizePolicy);
        addDiszipline->setFont(font);

        formLayout->setWidget(3, QFormLayout::FieldRole, addDiszipline);


        retranslateUi(ShooterSelectionView);

        QMetaObject::connectSlotsByName(ShooterSelectionView);
    } // setupUi

    void retranslateUi(QWidget *ShooterSelectionView)
    {
        ShooterSelectionView->setWindowTitle(QApplication::translate("ShooterSelectionView", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("ShooterSelectionView", "Name des Sch\303\274tzen: ", Q_NULLPTR));
        label_2->setText(QApplication::translate("ShooterSelectionView", "Neuen Sch\303\274tzen anlegen:", Q_NULLPTR));
        addShooter->setText(QApplication::translate("ShooterSelectionView", "+", Q_NULLPTR));
        label_3->setText(QApplication::translate("ShooterSelectionView", "Disziplin:", Q_NULLPTR));
        label_4->setText(QApplication::translate("ShooterSelectionView", "Neue Disziplin anlegen:", Q_NULLPTR));
        addDiszipline->setText(QApplication::translate("ShooterSelectionView", "+", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ShooterSelectionView: public Ui_ShooterSelectionView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOOTERSELECTIONVIEW_H
