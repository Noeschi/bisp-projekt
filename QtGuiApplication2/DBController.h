#pragma once

#include <qlist.h>
#include <qpair.h>
#include <qstring.h>
#include <qdatetime.h>
#include <qmap.h>
#include <memory.h>
#include "Datarow.h"
#include "DBManager.h"
#include "DataPackage.h"

typedef QMap<QString, QString> insert_params;



/**
* @brief The DBController class controlls the access to the database by using a DBManager
*/
class DBController {

public:

	/**
	* @brief Creates a new DBController 
	* @param path Name of the database to be initialized
	*/
	DBController(QString& path);
	
	/**
	* @brief Returns the different weapon types in the weapon_type table
	* @return QList with weapon types
	*/
	QList<QString> get_weapon_types();

	/**
	* @brief Return the id of a weapon type via its name
	* @param weapon_name name of the weapon which id should be returned from the database
	* @return id according to weapon_name
	*/
	int get_weapon_type_id_by_name(QString& weapon_name);

	/**
	* @brief Adds a weapon_type to the weapon_type table
	* @param weapon_name name of the weapon that should be added
	*/
	void add_weapon_type(QString& weapon_name);

	/**
	* @brief Returns the results of a shooter in a list via shooter's id
	* @param shooter_id id of the shooter whose  results should be returned
	* @return results as DataRows in a list
	*/
	QList<DataRow> get_results(int shooter_id);

	/**
	* @brief Returns the results of a shooter in a list via its first and last name
	* @param first_name first name of the shooter
	* @param last_name last name of the shooter
	* @return results as DataRows in a list
	*/
	QList<DataRow> get_results(QString& first_name, QString& last_name);

	/**
	* @brief returns all disciplines in the disciplines table
	* @return disciplines in a list
	*/
	QList<QString> get_disciplines();

	/**
	* @brief Returns the id of a discipline in the discipline table
	* @param weapon name of the weapon belonging to the discipline
	*/
	int get_discipline_id(QString& weapon);

	/**
	* @brief Returns the id of a shooter via its first and last name
	* @param last_name last name of the shooter
	* @param first_name first name of the shooter
	* @return index of the shooter in shooter table
	*/
	int get_index_of_shooter(QString& last_name, QString& first_name);

	/**
	* @brief Returns the id of a shooter via its full name
	* @param full_name full name of the shooter "<first name> <last name>"
	* @return index of the shooter in shooter table
	*/
	int get_index_of_shooter(QString& full_name);

	/**
	* @brief Returns the name of a shooter via given shooter table index
	* @param index shooter table index of the shooter
	* @return full name of the shooter "<first name> <lasst name>"
	*/
	QString get_shooter(int index);

	/**
	* @brief Returns names off all shooters.
	* @return List of names of the shooters "<first name> <last name>"
	*/
	QList<QString> get_shooter_names();

	/**
	* @brief Returns names of the shooters as list of QString pairs
	* @return list of pairs of the shooters' names QPair(<first name>, <last name>)
	*/
	QList<QPair<QString, QString>> get_shooter_names_as_pair();

	/**
	* @brief Adds a new shooter to the shooter data base
	* @param first_name first name of the shooter as QString
	* @param last_name last name of the shooter as QString
	* @param laterality laterality of the shooter; possible QStrings are "linkshaendig" or "rechtshaendig"
	* @param gender Biological gender of the shooter; possible QStrings are "Maennlich" oder "Weiblich"
	* @param association Shooter association of the shooter
	* @param birth_date birth date of the shooter as QDate
	*/
	void add_shooter(QString& first_name, QString& last_name, QString& laterality, QString& gender, QString& association, QDate& birth_date);

	/**
	* @brief Returns list of the possible genders
	* @return List with genders
	*/
	QList<QString> get_genders();

	/**
	* @brief Returns list of lateralities
	* @return List of lateralities
	*/
	QList<QString> get_lateralities();

	/**
	* @brief
	* @return
	*/
	int last_id_of(QString& table_name);

	/**
	* @brief Gets shooting results belonging to a result id
	* @return List of list of results
	*/
	QList<QList<DataPackage>> get_data_of_result(int result_id);

protected:
	//DBManager* db_pointer_;/**< List for saving children ids*/
	std::unique_ptr<DBManager> db_pointer_;
	QList<DataRow> result_;
	QString path_;

	bool insert(QString& table, const insert_params & parameters);
};