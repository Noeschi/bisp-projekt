#include "WeaponCreationWidget.h"
#include "ui_WeaponCreationWidget.h"

WeaponCreationWidget::WeaponCreationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WeaponCreationWidget)
{
    ui->setupUi(this);
}

WeaponCreationWidget::~WeaponCreationWidget()
{
    delete ui;
}

QString WeaponCreationWidget::getWeaponType()
{
    return ui->weaponType->text();
}

float WeaponCreationWidget::getGauge()
{
    return ui->gauge->value();
}
