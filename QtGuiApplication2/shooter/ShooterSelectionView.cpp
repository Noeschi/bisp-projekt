#include "ShooterSelectionView.h"
#include "ui_ShooterSelectionView.h"

ShooterSelectionView::ShooterSelectionView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ShooterSelectionView)
{
    ui->setupUi(this);
    connect(ui->addShooter, SIGNAL(pressed()), this, SIGNAL(clickedAdd()));
    connect(ui->addDiszipline, SIGNAL(pressed()), this, SIGNAL(clickedAddDiscipline()));
}

ShooterSelectionView::~ShooterSelectionView()
{
    delete ui;
}

void ShooterSelectionView::setItems(QStringList names)
{
    ui->shooterName->clear();
    ui->shooterName->addItems(names);
}

void ShooterSelectionView::selectItem(QString name)
{
    ui->shooterName->setCurrentText(name);
}

void ShooterSelectionView::setDisciplines(QStringList disciplines)
{
    ui->discipline->clear();
    ui->discipline->addItems(disciplines);
}

void ShooterSelectionView::setCurrentDiscipline(QString discipline)
{
    ui->discipline->setCurrentIndex(ui->discipline->findText(discipline));
}

QString ShooterSelectionView::getShooter()
{
    return ui->shooterName->currentText();
}

int ShooterSelectionView::getDiscipline()
{
    return ui->discipline->currentIndex();
}
