#ifndef SHOOTERSELECTIONVIEW_H
#define SHOOTERSELECTIONVIEW_H

#include <QWidget>

namespace Ui {
class ShooterSelectionView;
}

/**
 * @brief The ShooterSelectionView class provides a frontend for selecting a shooter for the current session
 */
class ShooterSelectionView : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief creates a new ShooterSelectionView
     * @param parent optional parent widget
     */
    explicit ShooterSelectionView(QWidget *parent = 0);
    ~ShooterSelectionView();

    /**
     * @brief set the available shooter names
     * @param names list of shooter names
     */
    void setItems(QStringList names);

    /**
     * @brief set selected shooter name
     * @param name shooter name
     */
    void selectItem(QString name);

    /**
     * @brief setDisciplines
     * @param disciplines
     */
    void setDisciplines(QStringList disciplines);

    /**
     * @brief select a specific discipline
     * @param discipline discipline name
     */
    void setCurrentDiscipline(QString discipline);

    /**
     * @brief get the selected shooter
     * @return name of the selected shooter
     */
    QString getShooter();

    /**
     * @brief get index of the current discipline
     * @return discipline index
     */
    int getDiscipline();

private:
    Ui::ShooterSelectionView *ui;

signals:
    /**
     * @brief emitted if clicked on add Shooter
     */
    void clickedAdd();

    /**
     * @brief emitted if clicked on add discipline
     */
    void clickedAddDiscipline();
};

#endif // SHOOTERSELECTIONVIEW_H
