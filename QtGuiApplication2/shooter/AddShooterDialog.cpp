#include "AddShooterDialog.h"
#include "ui_AddShooterDialog.h"

AddShooterDialog::AddShooterDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddShooterDialog)
{
    ui->setupUi(this);
}

AddShooterDialog::~AddShooterDialog()
{
    delete ui;
}

void AddShooterDialog::setLateralities(QStringList names)
{
    ui->laterality->addItems(names);
}

void AddShooterDialog::setGenders(QStringList names)
{
    ui->gender->addItems(names);
}

void AddShooterDialog::setAssociations(QStringList names)
{
    ui->associationBox->addItems(names);
}

void AddShooterDialog::focusFirstName()
{
    ui->first_name->setFocus();
}

QString AddShooterDialog::getName()
{
    return ui->name->text();
}

QString AddShooterDialog::getFirstName()
{
    return ui->first_name->text();
}

QString AddShooterDialog::getLaterality()
{
    return ui->laterality->currentText();
}

QString AddShooterDialog::getGender()
{
    return ui->gender->currentText();
}

QString AddShooterDialog::getAssociation()
{
    return ui->associationBox->currentText();
}

QDate AddShooterDialog::getBirthdate()
{
    return ui->birthdate->date();
}
