#ifndef RESULTLOADERWIDGET_H
#define RESULTLOADERWIDGET_H

#include <qwidget.h>

namespace Ui {
class ResultLoaderWidget;
}

/**
 * @brief wrapper for a pair of strings
 */
typedef QPair<QString, QString> Tuple;

/**
  * @brief wrapper for a triple of strings
  */
typedef struct Triple{
    QString first;
    QString second;
    QString third;
    Triple(QString first, QString second, QString third) {
        this->first = first;
        this->second = second;
        this->third = third;
    }
} Triple;

/**
 * @brief The ResultLoaderWidget class provides a frontend for loading results out of the database
 */
class ResultLoaderWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief creates a new ResultLoaderWidget
     * @param parent optional parent widget
     */
    explicit ResultLoaderWidget(QWidget *parent = 0);
    ~ResultLoaderWidget();

    /**
     * @brief set the availabes shooters
     * @param names list of shooter names <br>
     *      name[i].first = first name
     *      name[i].second = last name
     */
    void setShooters(QList<Tuple> names);

    /**
     * @brief setResults
     * @param tests
     */
    void setResults(QList<Triple> tests);

private:
    Ui::ResultLoaderWidget *ui;

public slots:
    /**
     * @brief widget callback if the selected shooter changed
     * @param index selected row
     */
    void shooterCallback(int index, int);

    /**
     * @brief callback if the selected result changed
     */
    void resultCallback();

signals:
    /**
     * @brief emitted if the selected shooter changed
     */
    void shooterChanged(Tuple);

    /**
     * @brief emitted if the selected result changed
     */
    void resultSelectionChanged(QList<QString>);
};

#endif // RESULTLOADERWIDGET_H
