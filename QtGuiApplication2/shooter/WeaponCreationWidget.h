#ifndef WEAPONCREATIONWIDGET_H
#define WEAPONCREATIONWIDGET_H

#include <QWidget>

namespace Ui {
class WeaponCreationWidget;
}

/**
 * @brief The WeaponCreationWidget class provides a frontend to add a new weapon type
 */
class WeaponCreationWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief creates a new WeaponCreationWidget
     * @param parent optional parent widget
     */
    explicit WeaponCreationWidget(QWidget *parent = 0);
    ~WeaponCreationWidget();

    /**
     * @brief get the name of the new weapon type
     * @return name of weapon type
     */
    QString getWeaponType();

    /**
     * @brief get the weapon types gauge
     * @return gauge of the weapon type
     */
    float getGauge();

private:
    Ui::WeaponCreationWidget *ui;
};

#endif // WEAPONCREATIONWIDGET_H
