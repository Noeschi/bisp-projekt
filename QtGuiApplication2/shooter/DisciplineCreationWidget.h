#ifndef DISCIPLINECREATIONWIDGET_H
#define DISCIPLINECREATIONWIDGET_H

#include <QWidget>

namespace Ui {
class DisciplineCreationWidget;
}

/**
 * @brief The DisciplineCreationWidget class provides a frontend for adding a new discipline
 */
class DisciplineCreationWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief creates a new DisciplineCreationWidget
     * @param parent optional parent widget
     */
    explicit DisciplineCreationWidget(QWidget *parent = 0);
    ~DisciplineCreationWidget();

    /**
     * @brief set the available weapon types
     * @param weaponTypes list of weapon types
     */
    void setWeaponTypes(QStringList weaponTypes);

    /**
     * @brief set predefined targets from other disciplines
     * @param disciplines list of disciplines
     */
    void setTargets(QStringList disciplines);

    /**
     * @brief select a weapon type from the lits
     * @param weaponType name of weapon to select
     */
    void selectWeaponType(QString weaponType);

    /**
     * @brief get the distance for the discipline
     * @return distance in mm
     */
    int getDistance();

    /**
     * @brief get the selected weapon type
     * @return name of weapon type
     */
    QString getWeaponType();

private:
    Ui::DisciplineCreationWidget *ui;

signals:
    /**
     * @brief emitted if the selected predefined target changed
     */
    void targetChanged(QString);

    /**
     * @brief emitted if the distance changed
     */
    void inputChanged();

    /**
     * @brief emitted if the weapon type changed
     */
    void addWeaponType();
};

#endif // DISCIPLINECREATIONWIDGET_H
