#include "DisciplineCreationWidget.h"
#include "ui_DisciplineCreationWidget.h"

DisciplineCreationWidget::DisciplineCreationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DisciplineCreationWidget)
{
    ui->setupUi(this);

    connect(ui->weapon_type, SIGNAL(currentTextChanged(QString)), this, SIGNAL(inputChanged()));
    connect(ui->targetChooser, SIGNAL(currentTextChanged(QString)), this, SIGNAL(targetChanged(QString)));
    connect(ui->distance, SIGNAL(valueChanged(double)), this, SIGNAL(inputChanged()));
    connect(ui->addWeapon, SIGNAL(pressed()), this, SIGNAL(addWeaponType()));
}

DisciplineCreationWidget::~DisciplineCreationWidget()
{
    delete ui;
}

void DisciplineCreationWidget::setWeaponTypes(QStringList weaponTypes)
{
    ui->weapon_type->clear();
    ui->weapon_type->addItems(weaponTypes);
}

void DisciplineCreationWidget::setTargets(QStringList disciplines)
{
    ui->targetChooser->clear();
    ui->targetChooser->addItems(disciplines);
}

void DisciplineCreationWidget::selectWeaponType(QString weaponType)
{
    ui->weapon_type->setCurrentIndex(ui->weapon_type->findText(weaponType));
}

int DisciplineCreationWidget::getDistance()
{
    return ui->distance->value() * 1000;
}

QString DisciplineCreationWidget::getWeaponType()
{
    return ui->weapon_type->currentText();
}
