#ifndef ADDSHOOTERDIALOG_H
#define ADDSHOOTERDIALOG_H

#include <qwidget.h>
#include <qdatetime.h>

namespace Ui {
class AddShooterDialog;
}

/**
 * @brief The AddShooterDialog class provides a frontend for adding a new shooter
 */
class AddShooterDialog : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief creates a new AddShooterDialog
     * @param parent optional parent
     */
    explicit AddShooterDialog(QWidget *parent = 0);
    ~AddShooterDialog();

    /**
     * @brief set the available lateralities
     * @param names list of lateralities
     */
    void setLateralities(QStringList names);

    /**
     * @brief set the available genders
     * @param names list of genders
     */
    void setGenders(QStringList names);

    /**
     * @brief set the availabe associations
     * @param names list of associations
     */
    void setAssociations(QStringList names);

    /**
     * @brief set focus on the first name
     */
    void focusFirstName();

    /**
     * @brief get the shooters name
     * @return last name of the shooter
     */
    QString getName();

    /**
     * @brief get the shooters first name
     * @return first name of the shooter
     */
    QString getFirstName();

    /**
     * @brief get the shooters laterality
     * @return laterality of the shooter
     */
    QString getLaterality();

    /**
     * @brief get the shooters gender
     * @return gender of the shooter
     */
    QString getGender();

    /**
     * @brief get the shooters association
     * @return association of the shooter
     */
    QString getAssociation();

    /**
     * @brief get the shooters birthdate
     * @return birthdate of the shooter
     */
    QDate getBirthdate();

private:
    Ui::AddShooterDialog *ui;

public slots:
};

#endif // ADDSHOOTERDIALOG_H
