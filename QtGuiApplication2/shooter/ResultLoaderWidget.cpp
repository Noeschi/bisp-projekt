#include "ResultLoaderWidget.h"
#include "ui_ResultLoaderWidget.h"

ResultLoaderWidget::ResultLoaderWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResultLoaderWidget)
{
    ui->setupUi(this);
    ui->testTable->setColumnHidden(2, true);

    ui->testTable->horizontalHeader()->setStretchLastSection(true);
    ui->testTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->testTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->testTable->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->shooterTable->horizontalHeader()->setStretchLastSection(true);
    ui->shooterTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->shooterTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->shooterTable->setSelectionMode(QAbstractItemView::SingleSelection);

    connect(ui->shooterTable, SIGNAL(cellClicked(int,int)), this, SLOT(shooterCallback(int,int)));
    connect(ui->testTable, SIGNAL(itemSelectionChanged()), this, SLOT(resultCallback()));
}

ResultLoaderWidget::~ResultLoaderWidget()
{
    delete ui;
}

void ResultLoaderWidget::setShooters(QList<QPair<QString, QString> > names)
{
    ui->shooterTable->setRowCount(names.size());
    for (int i = 0; i < names.size(); i++) {
        ui->shooterTable->setItem(i, 0, new QTableWidgetItem(names[i].first));
        ui->shooterTable->setItem(i, 1, new QTableWidgetItem(names[i].second));
    }
}

void ResultLoaderWidget::setResults(QList<Triple> tests)
{
    ui->testTable->setRowCount(tests.size());
    for (int i = 0; i < tests.size(); i++) {
        ui->testTable->setItem(i, 0, new QTableWidgetItem(tests[i].first));
        ui->testTable->setItem(i, 1, new QTableWidgetItem(tests[i].second));
        ui->testTable->setItem(i, 2, new QTableWidgetItem(tests[i].third));
    }
}

void ResultLoaderWidget::shooterCallback(int index, int)
{
    emit shooterChanged(Tuple(ui->shooterTable->item(index, 0)->text(), ui->shooterTable->item(index, 1)->text()));
}

void ResultLoaderWidget::resultCallback()
{
    QList<QString> ids;
    for (QTableWidgetItem *item: ui->testTable->selectedItems()) {
        if (item->column() == 0) {
            ids.push_back(ui->testTable->item(item->row(), 2)->text());
        }
    }
    emit resultSelectionChanged(ids);
}
