#pragma once
#include <QtSql/qtsqlversion.h>
#include <QtSql/qsqldatabase.h>
#include <qstring.h>
#include <string>
#include "Datarow.h"


/**
* @brief The DBManager class is used to load, save initiliaze (from) the database
*/
class DBManager {
public:

	/**
	* @brief Constructor
	* @param db_path name of the database
	*/
	DBManager (QString& db_path);

	/**
	* @brief execute_sql executes a sql statement and saves the result in rows
	* @param sql_statement sql statement to execute
	* @param rows saves result of statement 
	* @return true if executed correctly
	*/
	bool execute_sql(QString sql_statement, QList<DataRow>& rows);

	/**
	* @brief execute_sql executes a sql statement
	* @param sql_statement sql statement to execute
	* @return true if executed correctly
	*/
	bool execute_sql(QString sql_statement);

protected: 
	QString db_path_;/**< Name/path to the database */
	QSqlDatabase database_;/**< Database object */

	/**
	* @brief init_db initializes a database if not already done
	*/
	void init_db();

	/**
	* @brief print_error prints latest error from database_
	*/
	void print_error();
};