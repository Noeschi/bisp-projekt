#ifndef LINE_H
#define LINE_H

#include "vertex.h"
class Line2D
{
public:

    /**
     * @brief constructor
     * @param start starting vertice of the line
     * @param end ending vertice of the line
     * @param color color of the line
     */
    Line2D(Vertex2D &start, Vertex2D &end, QColor& color);

    /**
     * @brief standard constructor
     */
    Line2D();

    /**
     * @brief overloaded = operator
     * @param lineElement right element line that should be assigned
     * @return assigned line
     */
    Line2D& operator=(const Line2D& lineElement);

    /*************************************** getter **********************************/

    /**
     * @brief return starting vertice of line
     * @return start_
     */
    Vertex2D getStart()const;

    /**
     * @brief returns ending vertice of line
     * @return end_
     */
    Vertex2D getEnd() const;

    /**
     * @brief returns color of line
     * @return color_
     */
    QColor getColor() const;




private:
    Vertex2D start_;/**< starting vertice of the line*/
    Vertex2D end_;/**< ending vertice of the line */
    QColor color_;/**< color of the line */

};

#endif // LINE_H
