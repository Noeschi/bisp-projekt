#include "DBManager.h"
#include "FileInput.h"
#include <qfile.h>
#include <QtSql/qsqlquery.h>
#include <QtSql/qsqlrecord.h>
#include <QtSql/qsqlerror.h>
#include <qvariant.h>
#include <iostream>
#include <string>
#include <qjsondocument.h>


#define SQL_INIT "init.sql"

DBManager::DBManager(QString & db_path):
	db_path_(db_path)
{
	//check if database is already existing
	bool is_initiated = QFile(db_path_).exists();

	database_ = QSqlDatabase::addDatabase("QSQLITE");

	database_.setDatabaseName(db_path_);

	if (!database_.isValid()) {
		print_error();
	}

	if (!database_.open()) {
		std::cerr << "Datenbank konnte nicht ge�ffnet werden." << std::endl;
		exit(1);
	}

	QString sql_pragma = "PRAGMA Foreign_key = ON";//cascade deletion
	
	execute_sql(sql_pragma);
	// init database if not already existing
	if (!is_initiated) {
		init_db();
	}
}

bool DBManager::execute_sql(QString sql_statement, QList<DataRow>& rows)
{
	//clear rows 
	if (!rows.empty()) {
		rows.clear();
	}
	// execute sql query
	QSqlQuery query(sql_statement, database_);

	//return if an error occured
	if (database_.lastError().isValid()) {
		print_error();
		return false;
	}

	// push all values which rows is containing into a DataRow
	while (query.next()) {
		DataRow row;
		QSqlRecord record = query.record();
		for (int query_index = 0; query_index < query.record().count(); query_index++) {
			row.add(query.record().fieldName(query_index), query.value(query_index).toString());
		}
		
		rows.push_back(row);
		

	}


	return true;
}

bool DBManager::execute_sql(QString sql_statement)
{
	QList<DataRow> ret_list;

	QSqlQuery query(sql_statement, database_);

	if (database_.lastError().isValid()) {
		print_error();
		return false;
	}

	while (query.next()) {
		DataRow row;
		QSqlRecord record = query.record();
		for (int query_index = 0; query_index < query.record().count(); query_index++) {
			row.add(query.record().fieldName(query_index), query.value(query_index).toString());
		}

		ret_list.push_back(row);


	}

	return true;
}


void DBManager::init_db()
{
	QString sql_query;
	bool success(true);
	QList<DataRow> ret_list;
	
	//load the SQL_init file line by line 
	for (QString& line : FileInput::loadLines(SQL_INIT)) {

		//remove comments
		if (line.trimmed().startsWith("//")) {
			continue;
		}

		sql_query += line;
		//sanity check
		if (line.trimmed().endsWith(";")) {
			if (!sql_query.trimmed().isEmpty()) {
				std::cout << "Database Manager init " << sql_query.toStdString() << std::endl;
				//sp�ter checken ob das so hinhaut
				success &= execute_sql(sql_query);
			}
			sql_query.clear();
		}
	}

	std::cout << "Erfolgreich: " << success << std::endl;
}

void DBManager::print_error()
{
	std::cerr << database_.lastError().text().toStdString() << std::endl;
}
