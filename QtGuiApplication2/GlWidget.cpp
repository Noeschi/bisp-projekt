#include "GlWidget.h"
#include "line.h"
#include "Makros.h"

#include <QtOpenGL\qglfunctions.h>
#include <gl\GL.h>
#include <GL\GLU.h>



gl_widget::gl_widget(QWidget * parent): 
	QOpenGLWidget(parent)
{
	int k = 0;
}

gl_widget::~gl_widget()
{
}

void gl_widget::initializeGL()
{
	QOpenGLFunctions *gl_func = QOpenGLContext::currentContext()->functions();
	//empty clor buffer and define the new color
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	update();
}

void gl_widget::paintGL()
{
	//clear depth and color buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//reset matrix to identity matrix
	glLoadIdentity();
	//draw the coordinate system 
	draw();
}

void gl_widget::resizeGL(int width, int height)
{

	int side = qMin(width, height);
	//reset the view port
	glViewport(0, 0, width, height);
	//reset the projection matrix; this is done to be safe
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	update();
	draw();
}

void gl_widget::draw()
{
	static bool init(false);
	static int counter(0);
	
	if (!init) {
		//init = true;
		glClear(GL_COLOR_BUFFER_BIT);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		draw_coordinate_system();
		counter++;
	}
	draw_vertices();
	
}

void gl_widget::draw_coordinate_system()
{
	//vertices for vertical line
	Vertex2D left_upper_corner(QVector2D(-0.999f, 1.0f));
	Vertex2D left_lower_corner(QVector2D(-0.999f, -1.0f));
	//vertices for horizontal line
	Vertex2D left_zero_vertex(QVector2D(-1.0f, 0.0f));
	Vertex2D right_zero_vertex(QVector2D(1.0f, 0.0f));
	//create both lines 
	Line2D vertical_line(left_upper_corner, left_lower_corner, QColor((Qt::lightGray)));
	Line2D horizontal_line(left_zero_vertex, right_zero_vertex, QColor((Qt::lightGray)));

	//draw lines
	glBegin(GL_LINE_STRIP);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(vertical_line.getStart().position().x(), vertical_line.getStart().position().y(), 0);
		glVertex3f(vertical_line.getEnd().position().x(), vertical_line.getEnd().position().y(), 0);
	glEnd();
	glBegin(GL_LINE_STRIP);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(horizontal_line.getStart().position().x(), horizontal_line.getStart().position().y(), 0);
		glVertex3f(horizontal_line.getEnd().position().x(), horizontal_line.getEnd().position().y(), 0);
	glEnd();

}

void gl_widget::draw_vertices()
{
	//return if there are less than two vertices existing
	if (vertices_.size() < 2 || vertices_.empty()) return;

	// current vertice
	Vertex2D current = vertices_.front();
	//saves first vertex temporarely 
	Vertex2D first = vertices_.front();
	vertices_.pop_front();

	// draw a line between every vertex and its predecessor
	for (Vertex2D& vertex : vertices_) {

		draw_line(current, vertex);
		current = vertex;
	}
	// push the first vertex back on the list
	vertices_.push_front(first);
}

void gl_widget::draw_line(Vertex2D & first_vertex, Vertex2D & second_vertex)
{
	glBegin(GL_LINE_STRIP);
		//float r = 
		glColor3f(first_vertex.color().x(), first_vertex.color().y(), first_vertex.color().z());
		// division by 5 is a bit arbitrary but necessary to put it on screen
		glVertex3f(first_vertex.position().x(), first_vertex.position().y()/5, 0);
		glVertex3f(second_vertex.position().x(), second_vertex.position().y()/5, 0);
	glEnd();
	update();
}

void gl_widget::add_meassured_value(float value) {
	
	// denotes the number of vertices that should be deleted
	// because they are no longer on the screen
	unsigned int delete_counter(0);
	
	for (Vertex2D& vertex : vertices_) {
		// check if verteex is still visible, if not increase counter
		if (vertex.position().x() < -1.0f)delete_counter++;
	}
	
	for (unsigned int index = 0; index < delete_counter; index++) {
		// pop "unseen" vertices from the vertex list
		vertices_.pop_front();
	}

	for (Vertex2D & vertex : vertices_) {
		
		float y = vertex.position().y();
		//set the x position a bit more left so that the vertices "move"
		// during the meassurement
		float x = vertex.position().x() - 0.1;
		vertex.setPosition(QVector2D(x,y));
	}
	// new vertex is set on the right side
	Vertex2D new_vertex(QVector2D(0.50f, value));
	vertices_.push_back(new_vertex);
	update();
	//draw();
}

QSize gl_widget::minimum_size_hint() const
{
	return QSize(300, 300);
}

QSize gl_widget::size_hint() const
{
	return QSize(300, 300);
}

evaluation_widget::evaluation_widget(QMap<unsigned int, QList<float>>& channel_meassurements, float frequency, QWidget * parent ) :
	gl_widget(parent), channel_meassurements_(channel_meassurements_), frequency_(frequency)
{
}

evaluation_widget::evaluation_widget(QWidget * parent)
{
}

evaluation_widget::~evaluation_widget()
{
}

void evaluation_widget::set_channel_meassurements(QMap<unsigned int, QList<float>>& channel_meassurements)
{
	convert_meassurements_into_vertices(channel_meassurements);
}

void evaluation_widget::set_frequency(float frequency)
{
	if (frequency != 0.0f) {
		frequency_ = frequency;
	}
}

void evaluation_widget::draw_vertices()
{


	QMap<unsigned int, QList<Vertex2D>>::iterator map_iter(channel_vertices_.begin()), map_end(channel_vertices_.end());
	// iterate over all channel meassurements and return if one of the is empty or contains less than two values 
	for (; map_iter != map_end; map_iter++) {
		if (map_iter.value().size() < 2 || map_iter.value().empty()) {
			return;
		}
	}

	map_iter = channel_vertices_.begin();

	for (; map_iter != map_end; map_iter++) {
		float time = 0.0f;
		
		QList<Vertex2D>::iterator m_list_iter(map_iter.value().begin()), m_list_end(map_iter.value().end());
		
		Vertex2D last_vertex(*m_list_iter);
		Vertex2D next_vertex(*m_list_iter);
		m_list_iter++;

		if (map_iter.key() != SHOT_SIGNAL_CHANNEL) {

			for (; m_list_iter != m_list_end; m_list_iter++) {
				draw_line(last_vertex, *m_list_iter);
				last_vertex = *m_list_iter;
			}
			
		}

		if (map_iter.key() == SHOT_SIGNAL_CHANNEL) {

			QList<Vertex2D>::iterator sec_iter(map_iter.value().begin()+1);
			unsigned int counter(0);
			for (; m_list_iter != m_list_end - 3; ) {
				last_vertex = *(sec_iter);
				draw_line(last_vertex, *(m_list_iter-1));
				counter += 2;
				m_list_iter += 2;
				sec_iter += 2;
			}

		}





	}
}

QVector3D evaluation_widget::get_color_for_channel(unsigned int channel_number)
{
	QVector3D return_color;
	switch (channel_number)
	{
	case 1: {
		return_color = QVector3D(1.0f, 0.0f, 0.0f);//red
		break;
	}
	case 2: {
		return_color = QVector3D(0.0f, 0.0f, 1.0f);//blue
		break;
	}
	case 3: {
		return_color = QVector3D(0.0f,1.0f,0.0f);//green
		break;
	}
	case 4: {
		return_color = QVector3D(1.0f, 1.0f, 0.0f);//yellow
		break;
	}
	default:
		return_color = QVector3D(0.0f, 0.0f, 0.0f);//black
		break;
	}


	return return_color;
}

void evaluation_widget::draw() {


	static bool init(false);
	static int counter(0);

	if (!init) {
		//init = true;
		glClear(GL_COLOR_BUFFER_BIT);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		draw_coordinate_system();
		counter++;
	}
	draw_vertices();


}

void evaluation_widget::paintGL()
{
	//clear depth and color buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//reset matrix to identity matrix
	glLoadIdentity();
	//draw the coordinate system 
	draw();
}

void evaluation_widget::select_shooting_signals()
{
	QList<Vertex2D>::iterator vert_iter(channel_vertices_[SHOT_SIGNAL_CHANNEL].begin()), vert_end(channel_vertices_[SHOT_SIGNAL_CHANNEL].end());

	QList<Vertex2D> shoot_signals;

	for (; vert_iter != vert_end; vert_iter++) {

		if (vert_iter->position().y() > threshold_shoot_signal_) {
			shoot_signals.push_back(*vert_iter);
			
			//vertex which lies on the x-axis for drawing a line
			Vertex2D x0_vertex = *vert_iter;
			QVector2D position(vert_iter->position().x(), 0.0f);
			x0_vertex.setPosition(position);

			shoot_signals.push_back(x0_vertex);
		}
	}

	channel_vertices_[SHOT_SIGNAL_CHANNEL] = shoot_signals;
}

void evaluation_widget::select_start_signal()
{
	QList<Vertex2D>::iterator vert_iter(channel_vertices_[START_SIGNAL_CHANNEL].begin()), vert_end(channel_vertices_[START_SIGNAL_CHANNEL].end());

	QList<Vertex2D> shoot_signals;

	for (; vert_iter != vert_end; vert_iter++) {

		if (vert_iter->position().y() > threshold_start_signal_) {
			shoot_signals.push_back(*vert_iter);

			//vertex which lies on the x-axis for drawing a line
			Vertex2D x0_vertex = *vert_iter;
			QVector2D position(vert_iter->position().x(), 0.0f);
			x0_vertex.setPosition(position);

			shoot_signals.push_back(x0_vertex);
			break;
		}
	}

	channel_vertices_[START_SIGNAL_CHANNEL] = shoot_signals;
}

void evaluation_widget::set_threshold_start_signal(float threshold)
{
	threshold_start_signal_ = threshold;
}

void evaluation_widget::set_threshold_shoot_signal(float threshold)
{
	threshold_shoot_signal_ = threshold;
}

float evaluation_widget::get_threshold_start_signal()
{
	return threshold_start_signal_;
}

float evaluation_widget::get_threshold_shoot_signal()
{
	return threshold_shoot_signal_;
}

void evaluation_widget::convert_meassurements_into_vertices(QMap<unsigned int, QList<float>>& channel_meassurements)
{
	QMap<unsigned int, QList<float>>::iterator map_iter(channel_meassurements.begin()), map_end(channel_meassurements.end());

	for (; map_iter != map_end; map_iter++) {
		QList<float>::iterator list_iter(map_iter.value().begin()), list_end(map_iter.value().end());
		
		float time = 1.0f;//is the x value for opengl visualization, starts right-> therefore 1
		unsigned int counter = 0;

		for (; list_iter != list_end; list_iter++) {
			float frac = (1.0f / frequency_);
			time -= frac;
			counter++;

			Vertex2D vertex(QVector2D( time ,*list_iter));
			vertex.setColor(get_color_for_channel(map_iter.key()));

			channel_vertices_[map_iter.key()].push_back(vertex);
			
		}

	}

	select_shooting_signals();

	select_start_signal();
}
