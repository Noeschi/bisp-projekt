#include "Datarow.h"

DataRow::DataRow()
{
}

void DataRow::add(QString & key, QString & value)
{
	data_[key.toLower()] = value;
}

void DataRow::add(QString & key, int value)
{
	add(key, QString::number(value));
}

void DataRow::add(QString key, float value)
{
	add(key, QString::number(value));
}

void DataRow::add(QString key, bool value)
{
	add(key, QString::number(value));
}

QString DataRow::get(QString & key)
{
	return data_.contains(key.toLower()) ? data_[key.toLower()] : "";;
}

bool DataRow::operator==(DataRow const & rhs)
{
	if (data_.size() != rhs.data_.size()) {
		return false;
	}
	
	for (QString& key : this->data_.keys()) {
		if (!rhs.data_.contains(key) || this->data_[key] != rhs.data_[key]) {
			return false;
		}
	}

	return true;
}
