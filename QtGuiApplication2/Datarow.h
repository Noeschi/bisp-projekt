#pragma once
#include <qstring.h>
#include <qmap.h>


/**
* @brief The DataRow class is used as a wrapper of a QMap
*/
class DataRow {


public: 
	
	/**
	* @brief Standard constructor
	*/
	DataRow();

	/**
	* @brief Adds a key value pair to the data_ QMap
	* @param key key
	* @param value value
	*/
	void add(QString& key, QString& value);

	/**
	* @brief Adds a key value pair to the data_ QMap
	* @param key key
	* @param value value
	*/
	void add(QString& key, int value);

	/**
	* @brief Adds a key value pair to the data_ QMap
	* @param key key
	* @param value value
	*/
	void add(QString key, float value);

	/**
	* @brief Adds a key value pair to the data_ QMap
	* @param key key
	* @param value value
	*/
	void add(QString key, bool value);

	/**
	* @brief Returns the value to a given key
	* @param key key for which the value should be returned
	*/
	QString get(QString& key);

	/**
	* @brief Compares two DataRow objects, true if every key and every value are identical
	* @param rhs DataRow that should be compared
	* @return true if identical, false otherwise
	*/
	bool operator == (DataRow const& rhs);

protected:
	QMap<QString, QString> data_;/**< QMap for saving key-value pairs*/

};
