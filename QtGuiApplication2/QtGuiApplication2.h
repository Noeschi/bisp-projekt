#pragma once

#include <QtWidgets/QMainWindow>

#include "ui_QtGuiApplication2.h"

#include "WindowsProcedure.h"


/**
* @brief
*/
class QtGuiApplication2 : public QMainWindow
{
	Q_OBJECT

public:
	/**
	* @brief constructor
	* @param parent pointer to the parent widget
	*/
	QtGuiApplication2(QWidget *parent = Q_NULLPTR);

	void set_test_data_active(QList<float>& meassurement_list);

	void set_test_data_active(QMap<unsigned int, QList<float>>& meassurements);
	
private:

	Ui::QtGuiApplication2Class ui;
	
	channel_sender channel_sender;


protected:

	/**
	* @brief closeEvent contains some clean after the window is closed
	*/
	void closeEvent(QCloseEvent *event);



signals:

	/**
	* @brief the speichern button was clicked 
	*/
	void speichern_triggered();

	/**
	* @brief emit_load_channel_two emitted when load channel was one selected
	*/
	void emit_load_channel_one(std::string& path);

	/**
	* @brief emit_load_channel_two emitted when load channel was two selected
	*/
	void emit_load_channel_two(std::string& path);

	/**
	* @brief emit_load_channel_three is emitted when load channel three was selected
	*/
	void emit_load_channel_three(std::string& path);

	/**
	* @brief emit_load_channel_two emitted when load channel was four selected
	*/
	void emit_load_channel_four(std::string& path);

private slots:
	
	/**
	* @brief update_first_channel_value updates the text field for channel one
	*/
	void update_first_channel_value(float value);

	/**
	* @brief update_first_channel_value updates the text field for channel two
	* @param value value to be updated
	*/
	void update_second_channel_value(float value);

	/**
	* @brief update_first_channel_value updates the text field for channel three
	* @param value value to be updated
	*/
	void update_third_channel_value(float value);

	/**
	* @brief update_first_channel_value updates the text field for channel four
	* @param value value to be updated
	*/
	void update_fourth_channel_value(float value);
	
	/**
	* @brief update_elapsed_time updates the text field for the elapsed time since the beginning of the meassurement
	* @param elapsed_time time that passed
	*/
	void update_elapsed_time(int elapsed_time);

	/**
	* @brief update_amount_of_meassurements updates the text field for the number of meassurements
	* @param amount_meassurements number of meassurements
	*/
	void update_amount_of_meassurements(unsigned int amount_meassurements);
	
	/**
	* @brief on_speichern_triggered emits signal that speichern button was triggered 
	*/
	void on_speichern_triggered();

	/**
	* @brief on_stop_triggered empty atm 
	*/
	void on_stop_triggered();
	
	/**
	* @brief loads a file to be shown on the channel one widget
	*/
	void on_load_channel_one_triggered();

	/**
	* @brief loads a file to be shown on the channel two widget
	*/
	void on_load_channel_two_triggered();

	/**
	* @brief loads a file to be shown on the channel three widget
	*/
	void on_load_channel_three_triggered();

	/**
	* @brief loads a file to be shown on the channel four widget
	*/
	void on_load_channel_four_triggered();

	void evaluation_button_triggered();
	
signals:
	/**
	* @brief just for testing reasons 
	*/
	void hello(float test);
};
