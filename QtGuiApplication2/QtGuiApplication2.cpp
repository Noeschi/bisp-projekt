#include "QtGuiApplication2.h"
#include "Datareader.h"
#include "Meassurement_analyzer.h"

#include<WindowsProcedure.h>
#include <qtime>
#include <qthread.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qtextstream.h>



QtGuiApplication2::QtGuiApplication2(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	//connect(this, SIGNAL(externalDatapackageLoaded(Glovedata&)), ui->actualHandmodel, SLOT(sendLoadedGloveDataPackage(Glovedata&)));
	// connect signals for new values to slots which are displaying them on the ui
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_one(float)), this, SLOT(update_first_channel_value(float)));
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_two(float)), this, SLOT(update_second_channel_value(float)));
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_three(float)), this, SLOT(update_third_channel_value(float)));
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_four(float)), this, SLOT(update_fourth_channel_value(float)));

	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_one(float)), this, SLOT(update_first_channel_value(float)));
	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_two(float)), this, SLOT(update_second_channel_value(float)));
	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_three(float)), this, SLOT(update_third_channel_value(float)));
	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_four(float)), this, SLOT(update_fourth_channel_value(float)));
	
	// connect signal to slot for updating the time that elapsed since the beginning of a meassurement
	connect(&windows_procedure::get_instance().sender, SIGNAL( update_time(int)), this, SLOT( update_elapsed_time(int)));

	// connect signal to slot for displaying the number of meassurement 
	connect(&windows_procedure::get_instance().sender, SIGNAL(update_number_meassurements(unsigned int)), this, SLOT(update_amount_of_meassurements(unsigned int)));

	// connect new meassurements signal to slots where they are collected according to their channel
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_one(float)), ui.glwidget_one, SLOT(add_meassured_value_channel_one(float)));
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_two(float)), ui.glwidget_two, SLOT(add_meassured_value(float)));
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_three(float)), ui.glwidget_three, SLOT(add_meassured_value(float)));
	connect(&windows_procedure::get_instance().sender, SIGNAL(meassured_value_changed_channel_four(float)), ui.glwidget_4, SLOT(add_meassured_value(float)));

	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_one(float)), ui.glwidget_one, SLOT(add_meassured_value(float)));
	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_two(float)), ui.glwidget_two, SLOT(add_meassured_value(float)));
	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_three(float)), ui.glwidget_three, SLOT(add_meassured_value(float)));
	connect(&this->channel_sender, SIGNAL(meassured_value_changed_channel_four(float)), ui.glwidget_4, SLOT(add_meassured_value(float)));
	
	// connect the save signal for the meassurements 
	connect(ui.actionSpeichern, SIGNAL(triggered()), &windows_procedure::get_instance().sender, SLOT(safe_meassurements_to_file()));
	
	//connect evaluation window to evaluation button 

	connect(ui.evaluation_button, SIGNAL(clicked()), this, SLOT(evaluation_button_triggered()));


	// connect the load channel one signal 
	connect(ui.actionKanal_1, SIGNAL(triggered()), this, SLOT(on_load_channel_one_triggered()));

	emit(hello(1.f));
}

void QtGuiApplication2::update_first_channel_value(float value) {
	
	ui.channel_one->setText(QString::number(value));
	
}

void QtGuiApplication2::set_test_data_active(QList<float>& meassurement_list)
{
	channel_sender.set_test_data_active();
	channel_sender.set_test_data_to_send(1, meassurement_list);
}

void QtGuiApplication2::set_test_data_active(QMap<unsigned int, QList<float>>& meassurements)
{
	channel_sender.set_test_data_active();
	QMap<unsigned int, QList<float>>::iterator map_iter(meassurements.begin()), map_end(meassurements.end());

	for (; map_iter != map_end; map_iter++) {
		unsigned int channel = map_iter.key();
		QList<float> meass_list = map_iter.value();
		channel_sender.set_test_data_to_send(channel, meass_list);
	}
}

void QtGuiApplication2::closeEvent(QCloseEvent * event)
{
			
}

void QtGuiApplication2::update_second_channel_value(float value) {

	ui.channel_two->setText(QString::number(value));
}

void QtGuiApplication2::update_third_channel_value(float value)
{
	ui.channel_three->setText(QString::number(value));
}

void QtGuiApplication2::update_fourth_channel_value(float value)
{
	ui.channel_four->setText(QString::number(value));
}

void QtGuiApplication2::update_elapsed_time(int elapsed_time)
{
	ui.label_zeit->setText(QString::number(elapsed_time));
}

void QtGuiApplication2::update_amount_of_meassurements(unsigned int amount_meassurements)
{
	ui.label_messungen->setText(QString::number(amount_meassurements));
}



void QtGuiApplication2::on_speichern_triggered()
{
	emit(speichern_triggered());
	//windows_procedure::get_instance().stop_running();
}

void QtGuiApplication2::on_stop_triggered() {

}

void QtGuiApplication2::on_load_channel_one_triggered()
{
	
	QString qstring_path = QFileDialog::getOpenFileName(this, "Kanal 1 �ffnen");
	std::string path = qstring_path.toStdString();

	windows_procedure::get_instance().disable_live_data();

	emit(emit_load_channel_one(path));
}

void QtGuiApplication2::on_load_channel_two_triggered()
{
	QString qstring_path = QFileDialog::getOpenFileName(this, "Kanal 2 �ffnen");
	std::string path = qstring_path.toStdString();

	windows_procedure::get_instance().disable_live_data();

	emit(emit_load_channel_two(path));
}

void QtGuiApplication2::on_load_channel_three_triggered()
{
	QString qstring_path = QFileDialog::getOpenFileName(this, "Kanal 3 �ffnen");
	std::string path = qstring_path.toStdString();

	windows_procedure::get_instance().disable_live_data();

	emit(emit_load_channel_three(path));
}

void QtGuiApplication2::on_load_channel_four_triggered()
{
	QString qstring_path = QFileDialog::getOpenFileName(this, "Kanal 4 �ffnen");
	std::string path = qstring_path.toStdString();

	windows_procedure::get_instance().disable_live_data();

	emit(emit_load_channel_four(path));
}

void QtGuiApplication2::evaluation_button_triggered()
{
	
	evaluation_widget* eval_widget = new evaluation_widget();

	data_reader reader;
	QList<float> c1 = reader.load_data_from_file("Kanal_1.txt", ';');
	QList<float> c2 = reader.load_data_from_file("Kanal_2.txt", ';');
	QList<float> c3 = reader.load_data_from_file("Kanal_3.txt", ';');
	QList<float> c4 = reader.load_data_from_file("Kanal_4.txt", ';');

	QMap<unsigned int, QList<float>> test_meassurements;

	test_meassurements[1] = c1;
	test_meassurements[2] = c2;
	test_meassurements[3] = c3;
	test_meassurements[4] = c4;

	//(float threshold_start_signal, float threshold_pressure_signal, unsigned int frequence, unsigned int time_of_meassurement, unsigned int offset)
	meassurement_analyzer mess_anal(5.0f, 2.0f, 100, 4, 20);

	mess_anal.trimm_meassurement(test_meassurements);
	
	eval_widget->set_threshold_shoot_signal(mess_anal.get_threshold_shot_signal());

	eval_widget->set_threshold_start_signal(mess_anal.get_threshold_start_signal());
	
	eval_widget->set_frequency(test_meassurements[1].size() * 0.5f );
	
	eval_widget->set_channel_meassurements(test_meassurements);
	
	this->hide();
	eval_widget->show();
	
}

