#include "WindowsProcedure.h"
#include <stdio.h>
#include <conio.h>
#include <QtConcurrent\qtconcurrentrun.h>
//#include <QElapsedTimer>
#include <qelapsedtimer.h>
#include <qthread.h>
#include <iostream>
#include <fstream>



#define CHECKERROR( ecode )                           \
do                                                    \
{                                                     \
   ECODE olStatus;                                    \
   if( OLSUCCESS != ( olStatus = ( ecode ) ) )        \
   {                                                  \
      printf("OpenLayers Error %d\n", olStatus );  \
      exit(1);                                        \
   }                                                  \
}                                                     \
while(0)      
//kanl 1 druckverlauf, kanal zwei speedograph, kanal drei schussmikro, kanal 4 startsignal
#define NUM_OL_BUFFERS 10
#define TOTAL_CHANNEL 4
#define SAMPLE_FREQUENCY_PER_CHANNEL 100 // Acquistion speed per channel 
#define BUFFERS_PER_SECOND 10

double min, max;
unsigned int encoding, resolution;

windows_procedure::~windows_procedure() {
	stop_running();
}


LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM hAD, LPARAM lParam)
{
	unsigned long samples, value;
	unsigned int i;
	// define two pointers for using devices with resolution up to 32-bit
	unsigned int * pBuffer32 = NULL;
	unsigned short int  *pBuffer = NULL;
	double volts;
	switch (msg)
	{
	case OLDA_WM_BUFFER_DONE:
		
		//if (!windows_procedure::get_instance().get_live_data_enabled()));


		// first try to stop the data acquisition 
		if (!windows_procedure::get_instance().get_live_data_enabled()) {
			//olDaStop(windows_procedure::get_instance().hAD);
			//olDaAbort(windows_procedure::get_instance().hAD);
			//olDaTerminate(windows_procedure::get_instance().hDev);
			SendMessage(windows_procedure::get_instance().hWnd_, WM_QUIT,0,0);
			windows_procedure::get_instance().stop_running_ = true;
			return 0;
		}
		HBUF hBuf;

		// Get the handle to the buffer
		olDaGetBuffer((HDASS)hAD, &hBuf);

		// read out the number of valid samples in the buffer
		olDmGetValidSamples(hBuf, &samples);

		if (samples > 0)
		{

			// get pointer to the buffer depending to the resolution
			if (resolution > 16)
				olDmGetBufferPtr(hBuf, (LPVOID*)&pBuffer32);
			else
				olDmGetBufferPtr(hBuf, (LPVOID*)&pBuffer);


			// release the buffer for new data
			olDaPutBuffer((HDASS)hAD, hBuf);

			// display the value in the console
			for (i = 0; i<samples; i++)
			{
				
				if (resolution > 16)
					value = pBuffer32[i];
				else
					value = pBuffer[i];

				//  convert the last value in the buffer to volts
				olDaCodeToVolts(min, max, 1.0, resolution, encoding, value, &volts);

				printf("Last value of the buffer for channel %d: %f \n", i - (samples - TOTAL_CHANNEL), volts);
				//number of the channel
				unsigned int channel = i%TOTAL_CHANNEL;
				qDebug() <<"Channel "  << channel  << " Volt " << volts;
				
				
				std::pair<unsigned int, float> channel_value(channel, volts);
				// emit signal containing the channel/meassurement pair 
				windows_procedure::get_instance().emit_signal_to_channel(channel_value);
			
			}

		}

		break;
	// these are standard cases from the documentation
	case OLDA_WM_QUEUE_DONE:
		printf("\nAcquisition stopped, rate too fast for current options.");
		PostQuitMessage(0);
		break;

	case OLDA_WM_TRIGGER_ERROR:
		printf("\nTrigger error: acquisition stopped.");
		PostQuitMessage(0);
		break;

	case OLDA_WM_OVERRUN_ERROR:
		printf("\nInput overrun error: acquisition stopped.");
		PostQuitMessage(0);
		break;

	case WM_QUIT:
		return 0;

	default:
		return DefWindowProc(hWnd, msg, hAD, lParam);
	}

	return 0;
}



BOOL CALLBACK EnumBrdProc(LPSTR lpszBrdName, LPSTR lpszDriverName, LPARAM lParam)
{

	// Make sure we can Init Board
	if (OLSUCCESS != (olDaInitialize((PTSTR)lpszBrdName, (LPHDEV)lParam)))
	{
		return TRUE;  // try again
	}

	// Make sure Board has an A/D Subsystem 
	UINT uiCap = 0;
	olDaGetDevCaps(*((LPHDEV)lParam), OLDC_ADELEMENTS, &uiCap);
	if (uiCap < 1)
	{
		return TRUE;  // try again
	}

	printf("%s succesfully initialized.\n", lpszBrdName);
	return FALSE;    // all set , board handle in lParam
}

int windows_procedure::start_procedure()
{
	unsigned int i;
	unsigned int IsSimultaneous;
	unsigned int SampleSize, Samples;
	double SampleFrequency;
	
	printf("Open Layers Continuous A/D Win32 Console Example\n");

	



	hDev = NULL;
	// get all DT devices
	ECODE err = olDaEnumBoards((DABRDPROC)EnumBrdProc, (LPARAM)&hDev);
	CHECKERROR(olDaEnumBoards((DABRDPROC)EnumBrdProc, (LPARAM)&hDev));

	hAD = NULL;
	// get handle to subsystem
	CHECKERROR(olDaGetDASS(hDev, OLSS_AD, 0, &hAD));

	// read out important board properties for the calculation
	
	//  get maximum and minimum values of the device
	CHECKERROR(olDaGetRange(hAD, &max, &min));
	// get encoding of the device; standard procedure
	CHECKERROR(olDaGetEncoding(hAD, &encoding));
	// gets number of bits of resolution of the device 
	CHECKERROR(olDaGetResolution(hAD, &resolution));

	//check if simultaneous acquisition is possible
	//should not be case for the used A/D-wandler
	olDaGetSSCaps(hAD, OLSSC_SUP_SIMULTANEOUS_SH, &IsSimultaneous);
	if (IsSimultaneous == false)
		SampleFrequency = SAMPLE_FREQUENCY_PER_CHANNEL * TOTAL_CHANNEL;
	else
		SampleFrequency = SAMPLE_FREQUENCY_PER_CHANNEL;

	// sets the window that should receive the meassurement messages 
	CHECKERROR(olDaSetWndHandle(hAD, hWnd_, 0));
	//set the data flow as continuous(not single value)
	CHECKERROR(olDaSetDataFlow(hAD, OL_DF_CONTINUOUS));
	// set the size of the channel list; standard is four 
	CHECKERROR(olDaSetChannelListSize(hAD, TOTAL_CHANNEL));
	
	//determine the order of the channel list; it contains the meassurement values
	for (i = 0; i<TOTAL_CHANNEL; i++)
	{
		// sets the entry of the channel list
		// the order of the channel values in the channel list is: 1,2,3,4
		CHECKERROR(olDaSetChannelListEntry(hAD, i, i));
		CHECKERROR(olDaSetGainListEntry(hAD, i, i));
	}
	// defines the trigger of the data acquisition; here it is the software that starts it
	CHECKERROR(olDaSetTrigger(hAD, OL_TRG_SOFT));
	// use the clock of the device
	CHECKERROR(olDaSetClockSource(hAD, OL_CLK_INTERNAL));
	// set the the frequency of the meassurements on the device
	CHECKERROR(olDaSetClockFrequency(hAD, SampleFrequency));
	// use OL_WRP_NONE for gap free data
	CHECKERROR(olDaSetWrapMode(hAD, OL_WRP_NONE));
	// configure the subsystem with defined values
	CHECKERROR(olDaConfig(hAD));

	if (resolution <= 16)
		SampleSize = 2;
	else
		SampleSize = 4;

	if (IsSimultaneous == true)
		Samples = (SampleFrequency * TOTAL_CHANNEL) / BUFFERS_PER_SECOND;
	else
		Samples = SampleFrequency / BUFFERS_PER_SECOND;


	HBUF hBufs[NUM_OL_BUFFERS];
	for (int i = 0; i < NUM_OL_BUFFERS; i++)
	{
		//allocate buffers fot the data
		if (OLSUCCESS != olDmCallocBuffer(GHND, NULL, Samples, SampleSize, &hBufs[i]))
		{
			for (i--; i >= 0; i--)
			{
				olDmFreeBuffer(hBufs[i]);
			}
			exit(1);
		}
		// assign buffer to the ready queue
		olDaPutBuffer(hAD, hBufs[i]);
	}

	// try to start data acquisition
	if (OLSUCCESS != (olDaStart(hAD)))
	{
		printf("A/D Operation Start Failed...hit any key to terminate.\n");
	}
	else
	{
		printf("A/D Operation Started...hit any key to terminate.\n\n");
	}

	MSG msg;
	SetMessageQueue(50);                      // Increase the our message queue size so
											  // we don't lose any data acq messages

											  // Acquire and dispatch messages until a key is hit...since we are a console app 
											  // we are using a mix of Windows messages for data acquistion and console approaches
											  // for keyboard input.
	if (stop_running_)
	{
		return 0;
	}
	BOOL test;										  //

	while (test = GetMessage(&msg,        // message structure              
		hWnd_,        // handle of window receiving the message 
		0,           // lowest message to examine          
		0) > 0)        // highest message to examine         
	{

		TranslateMessage(&msg);    // Translates virtual key codes       
		DispatchMessage(&msg);     // Dispatches message to window 

		if (msg.message == WM_QUIT) {
			int k = 0;
			break;
		}

		if (_kbhit())
		{
			_getch();
			PostQuitMessage(0);
		}

	}
	// abort A/D operation
	olDaAbort(hAD);
	printf("\nA/D Operation Terminated \n");

	for (i = 0; i<NUM_OL_BUFFERS; i++)
	{
		//free the allocated buffer
		olDmFreeBuffer(hBufs[i]);
	}
	if (windows_procedure::get_instance().is_running()) {
		return 0;
	}
	//terminate the acquisition
	olDaTerminate(hDev);
	return 0;

}

void windows_procedure::emit_signal_to_channel(std::pair<unsigned int, float> channel_value)
{
	unsigned int channel = channel_value.first;
	static QElapsedTimer timer;
	static bool timer_init(false);// for time meassueremnt
	static unsigned int values = 0;// number of processed meassurements
	static unsigned int counter_meassurements(0);//counter for the meassurements
	//start timer if save meassurement is active
	if (safe_files_ && !timer_init) {
		timer_init = true;
		timer.start();
	}
	// check to which channel the value belongs
	switch (channel) {
	case 0: {
		// safe value to file if it is desired
		if (safe_files_) {
			std::ofstream file;
			file.open("Kanal_1.txt", std::ofstream::app);
			std::string test = std::to_string(channel_value.second) + ";";
			counter_meassurements++;
			file << test;
			file.close();
		}
		
		value_list_.push_back(channel_value.second);
		qDebug() << value_list_.size();
		// emit signal containing the newest meassurement
		emit(sender.meassured_value_changed_channel_one(channel_value.second));
		if (timer_init) {
			emit(sender.update_number_meassurements(counter_meassurements));
			emit(sender.update_time(timer.elapsed()));
		}

		break;
	}
	
	case 1: {
		if (safe_files_) {
			std::ofstream file;
			file.open("Kanal_2.txt", std::ofstream::app);
			std::string test = std::to_string(channel_value.second) + ";";
			file << test;
			file.close();
		}

		emit(sender.meassured_value_changed_channel_two(channel_value.second));
		break;
	}
	case 2: {
		if (safe_files_) {
			std::ofstream file;
			file.open("Kanal_3.txt", std::ofstream::app);
			std::string test = std::to_string(channel_value.second) + ";";
			file << test;
			file.close();
		}
		emit(sender.meassured_value_changed_channel_three(channel_value.second));
		break;
	}
	case 3: {
		if (safe_files_) {
			std::ofstream file;
			file.open("Kanal_4.txt", std::ofstream::app);
			std::string test = std::to_string(channel_value.second) + ";";
			file << test;
			file.close();
		}
		emit(sender.meassured_value_changed_channel_four(channel_value.second));
		break;
	}
	default: {
		break;
	}
	
	}
}

void windows_procedure::set_safe_files(bool safe)
{
	safe_files_ = safe;
}

void windows_procedure::stop_running()
{
	stop_running_ = true;
}

bool windows_procedure::is_running()
{
	return stop_running_;
}

void windows_procedure::enable_live_data()
{
	live_data_ = true;
}

void windows_procedure::disable_live_data()
{
	live_data_ = false;
}

bool windows_procedure::get_live_data_enabled()
{
	return live_data_;
}



windows_procedure & windows_procedure::get_instance()
{
	static windows_procedure instance;
	return instance;
}


void windows_procedure::set_window(HWND & hwnd)
{
	hWnd_ = hwnd;
}

HWND windows_procedure::get_window()
{
	return hWnd_;
}

void windows_procedure::set_window_class(WNDCLASS & wndclass)
{
	wc_ = wndclass;
	wc_.lpfnWndProc = WndProc;
}

WNDCLASS windows_procedure::get_window_class()
{
	return wc_;
}

void windows_procedure::start_thread()
{
	QFuture<void> t1 = QtConcurrent::run(this, & windows_procedure::start_procedure);
}

void channel_sender::safe_meassurements_to_file() {
	windows_procedure::get_instance().set_safe_files(true);
}

void channel_sender::send_out_test_data() {





	//windows_procedure::get_instance().emit_signal_to_channel(channel_value);



}


void data_acquisition_thread::run() {
	windows_procedure::get_instance().start_procedure();
}

channel_sender::channel_sender(bool test_data):
	test_data_(test_data)
{
	timer_ = new QTimer(0);
	timer_->setInterval(1000);
	connect(timer_, SIGNAL(timeout()), this, SLOT(send_out_test_meassurement_signal()));

	thread_ = new QThread(this);

	if (test_data_) {
		timer_->moveToThread(thread_);
		thread_->start();
	}
}

void channel_sender::set_test_data_active()
{
	timer_ = new QTimer(0);
	thread_ = new QThread(this);
	timer_->setInterval(100);
	timer_->moveToThread(thread_);
	this->connect(timer_, SIGNAL(timeout()), this, SLOT(send_out_test_meassurement_signal()), Qt::DirectConnection);

	
	test_data_ = true;
	timer_->connect(thread_, SIGNAL(started()), SLOT(start()));
	
	thread_->start();
	
}




void channel_sender::send_out_test_meassurement_signal()
{
	if ((channel_one_iterator_ != channel_one_list_.end()) && !channel_one_list_.empty()) {
		float value = *channel_one_iterator_;
		channel_one_iterator_++;
		emit(meassured_value_changed_channel_one(value));
	}
	else {
		emit(meassured_value_changed_channel_one(0.0f));
	}

	if ((channel_two_iterator_ != channel_two_list_.end()) && ! channel_two_list_.empty()) {
		float value = *channel_two_iterator_;
		channel_two_iterator_++;
		emit(meassured_value_changed_channel_two(value));
	}
	else {
		emit(meassured_value_changed_channel_two(0.0f));
	}

	if ((channel_three_iterator_ != channel_three_list_.end()) && !channel_three_list_.empty()) {
		float value = *channel_three_iterator_;
		channel_three_iterator_++;
		emit(meassured_value_changed_channel_three(value));
	}
	else {
		emit(meassured_value_changed_channel_three(0.0f));
	}

	if ((channel_four_iterator_ != channel_four_list_.end()) && !channel_four_list_.empty()) {
		float value = *channel_four_iterator_;
		channel_three_iterator_++;
		emit(meassured_value_changed_channel_four(value));
	}
	else {
		emit(meassured_value_changed_channel_four(0.0f));
	}

}

void channel_sender::set_test_data_to_send(unsigned int channel, QList<float> meassurement_list)
{
	switch (channel) {
		case 1: {
			channel_one_list_ = meassurement_list;
			
			if (!meassurement_list.empty()) {
				channel_one_iterator_ = channel_one_list_.begin();
			}

			break;
		}
		case 2: {
			channel_two_list_ = meassurement_list;

			if (!meassurement_list.empty()) {
				channel_two_iterator_ = channel_two_list_.begin();
			}

			break;
		}
		case 3: {
			channel_three_list_ = meassurement_list;

			if (!meassurement_list.empty()) {
				channel_three_iterator_ = channel_three_list_.begin();
			}

			break;
		}
		case 4: {
			channel_four_list_ = meassurement_list;

			if (!meassurement_list.empty()) {
				channel_four_iterator_ = channel_four_list_.begin();
			}

			break;
		}
		default: {
			break;
		}
	}

}
