#include "FileInput.h"

#include <qfile.h>
#include <qtextstream.h>
#include <qdir.h>

QStringList FileInput::loadLines(QString filepath)
{
	QStringList list;
	// open file it exists
	QFile file(filepath);
	if (file.exists()) {
		file.open(QIODevice::ReadOnly | QIODevice::Text);

		QTextStream stream(&file);
		//while there is a line in the file, load it
		while (!stream.atEnd()) {
			list.append(stream.readLine());
		}
	}

	return list;
}

QMap<QString, QString> FileInput::loadAsMap(QString filepath, QString seperator)
{
	QMap<QString, QString> data;
	// load file which contains for exampple values for multiple channels
	//key is the first separated word of a line
	for (QString line : loadLines(filepath)) {
		if (line.contains(seperator)) {
			QString key = line.left(line.indexOf(seperator)).trimmed();
			data[key] = line.right(line.length() - line.indexOf(seperator) - seperator.length()).trimmed();
		}
	}

	return data;
}

QStringList FileInput::listFiles(QString path)
{
	QDir dir(path);

	if (dir.exists())
		return dir.entryList(QDir::Files, QDir::Name);
	else
		return QStringList();
}
