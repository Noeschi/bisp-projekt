﻿    CREATE TABLE gender(
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL
        );

    CREATE TABLE laterality(
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL
        );

    CREATE TABLE association(
  	    short TEXT PRIMARY KEY,
        name TEXT NOT NULL
        );

    CREATE TABLE shooter(
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        first_name TEXT NOT NULL,
	      birthdate DATE NOT NULL,
        gender_id INTEGER NOT NULL,
        lat_id INTEGER NOT NULL,
        association TEXT NOT NULL,
        FOREIGN KEY(association) REFERENCES association(short),
        FOREIGN KEY(gender_id) REFERENCES gender(id) ON DELETE CASCADE,
        FOREIGN KEY(lat_id) REFERENCES laterality(id) ON DELETE CASCADE
        );

    CREATE TABLE weapon_type(
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL
        );

    CREATE TABLE discipline(
        id INTEGER PRIMARY KEY,
        weapon_type INTEGER NOT NULL,
        FOREIGN KEY(weapon_type) REFERENCES weapon_type(id) ON DELETE CASCADE
        );

    CREATE TABLE results(
        id INTEGER PRIMARY KEY,
        date DATETIME NOT NULL,
        shooter_id INTEGER NOT NULL,
        discipline_id INTEGER NOT NULL,
        test_id INTEGER NOT NULL,
        comment TEXT NOT NULL,
  //      squad_id INTEGER NOT NULL,
  //      style_id INTEGER NOT NULL,
        FOREIGN KEY(test_id) REFERENCES test(id) ON DELETE CASCADE,
        FOREIGN KEY(shooter_id) REFERENCES shooter(id) ON DELETE CASCADE,
        FOREIGN KEY(discipline_id) REFERENCES discipline(id) ON DELETE CASCADE
        );

    CREATE TABLE test_values(
        id INTEGER PRIMARY KEY,
        timestamp INTEGER NOT NULL,
        result_id INTEGER NOT NULL,
        measurement_nr INTEGER NOT NULL,
        FOREIGN KEY(result_id) REFERENCES results(id) ON DELETE CASCADE
        );

// Add Genders
    INSERT INTO gender(name)
        VALUES("Maennlich");

    INSERT INTO gender(name)
        VALUES("Weiblich");

//Add lateralities
    INSERT INTO laterality(name)
        VALUES("Linkshaendig");

    INSERT INTO laterality(name)
        VALUES("Rechtshaendig");

//Add weapon types
    INSERT INTO weapon_type(id, name)
        VALUES(1, "Luftpistole");
    INSERT INTO weapon_type(id, name)
        VALUES(2, "Sportpistole");
	INSERT INTO weapon_type(id, name)
        VALUES(3, "Schnellfeuerpistole");

//Add disciplines
    INSERT INTO discipline(weapon_type)
        VALUES(1);
    INSERT INTO discipline(weapon_type)
        VALUES(2);

//Add associations
    INSERT INTO association(name, short)
        VALUES("Wuertembergischer Schuetzenverband", "WT");
    INSERT INTO association(name, short)
        VALUES("Badischer Sportschuetzenbund", "BD");
    INSERT INTO association(name, short)
        VALUES("Bayerischer Sportschuetzenbund", "BY");
    INSERT INTO association(name, short)
        VALUES("Schuetzenverband Berlin-Brandenburg", "BL");
    INSERT INTO association(name, short)
        VALUES("Brandenburgischer Schuetzenbund", "BR");
    INSERT INTO association(name, short)
        VALUES("Schuetzenverband Hamburg und Umgegend", "HH");
    INSERT INTO association(name, short)
        VALUES("Hessischer Schuetzenverband", "HS");
    INSERT INTO association(name, short)
        VALUES("Landesschuetzenverband Mecklenburg-Vorpommern", "MV");
    INSERT INTO association(name, short)
        VALUES("Niedersaechsischer Sportschuetzenverband", "NS");
    INSERT INTO association(name, short)
        VALUES("Norddeutscher Schuetzenbund", "ND");
    INSERT INTO association(name, short)
        VALUES("Nordwestdeutscher Schuetzenbund", "NW");
    INSERT INTO association(name, short)
        VALUES("Oberpfaelzer Schuetzenbund", "OP");
    INSERT INTO association(name, short)
        VALUES("Pfaelzischer Sportschuetzenbund", "PF");
    INSERT INTO association(name, short)
        VALUES("Rheinischer Schuetzenbund", "RH");
    INSERT INTO association(name, short)
        VALUES("Schuetzenverband Saar", "SA");
    INSERT INTO association(name, short)
        VALUES("Landesschuetzenverband Sachsen-Anhalt", "ST");
    INSERT INTO association(name, short)
        VALUES("Saechsischer Schuetzenbund", "SC");
    INSERT INTO association(name, short)
        VALUES("Suedbadischer Sportschuetzenverband", "SB");
    INSERT INTO association(name, short)
        VALUES("Thueringer Schuetzenbund", "TH");
    INSERT INTO association(name, short)
        VALUES("Westfaelischer Schuetzenbund", "WF");
