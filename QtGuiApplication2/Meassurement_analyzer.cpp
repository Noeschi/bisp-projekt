#include "Meassurement_analyzer.h"
#include "Makros.h"
meassurement_analyzer::meassurement_analyzer()
{
}

meassurement_analyzer::meassurement_analyzer(float threshold_start_signal, float threshold_pressure_signal, unsigned int frequence,
	unsigned int time_of_meassurement, unsigned int offset) : threshold_start_signal_(threshold_start_signal), threshold_shot_signal_(threshold_pressure_signal),
	frequence_(frequence), time_of_meassurement_(time_of_meassurement), offset_(offset)
{

}

meassurement_analyzer::~meassurement_analyzer()
{
}

unsigned int meassurement_analyzer::get_start_index(QList<float>& meass_list)
{
	QList<float>::iterator list_iter(meass_list.begin()), list_end(meass_list.end());

	unsigned int start(0);

	for (; list_iter != list_end; list_iter++) {

		if (*list_iter > threshold_start_signal_) {
			start = std::distance(meass_list.begin(), list_iter);
			return start;
		}

	}
}

float meassurement_analyzer::get_max_pressure(QList<float>& press_values)
{
	
	auto it = std::max_element(std::begin(press_values), std::end(press_values));

	return *it;
}

float meassurement_analyzer::get_threshold_start_signal()
{
	return threshold_start_signal_;
}

float meassurement_analyzer::get_threshold_shot_signal()
{
	return threshold_shot_signal_;
}

void meassurement_analyzer::set_threshold_start_signal(float threshold)
{
	threshold_start_signal_ = threshold;
}

void meassurement_analyzer::set_threshold_shot_signal(float threshold)
{
	threshold_shot_signal_ = threshold;
}

void meassurement_analyzer::set_time_of_meassurement(unsigned int time)
{
	time_of_meassurement_ = time;
}

void meassurement_analyzer::set_frequence(unsigned int frequence)
{
	frequence_ = frequence;
}

void meassurement_analyzer::set_offset(unsigned int offset)
{
	offset_ = offset;
}

QMap<unsigned int, QList<float>> meassurement_analyzer::trimm_meassurement(QMap<unsigned int, QList<float>>& meassurement_map)
{

	unsigned int start = get_start_index(meassurement_map[START_SIGNAL_CHANNEL]);

	unsigned int end = start + time_of_meassurement_ * frequence_ + offset_;

	start -= offset_;

	QMap<unsigned int, QList<float>>::iterator map_iter(meassurement_map.begin()), map_end(meassurement_map.end());

	for (; map_iter != map_end; map_iter++) {

		QList<float>::iterator list_begin(map_iter.value().begin()), list_iter(map_iter.value().begin() + start - offset_);

		map_iter.value().erase(list_begin, list_iter);

		list_iter = map_iter.value().begin() + end;

		map_iter.value().erase(list_iter, map_iter.value().end());

	}
	


	return meassurement_map;
}
