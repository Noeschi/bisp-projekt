#include "Datareader.h"

#include <qdebug>
#include <qiodevice.h>
#include <fstream>
#include <sstream>

data_reader::data_reader()
{
}

QList<float> data_reader::load_data_from_file(const QString & path,  const char delim) const
{
	//open file
	std::istringstream in_file(path.toStdString());
	std::string line;
	QList<float> meassurements_list;
	
	//while there is still a line in the file, read it
	while (std::getline(in_file, line)) {
		QList<QString> splitted_input(split_line_into_values(QString::fromStdString(line), &delim));
		//convert list of strings to list of float values
		meassurements_list = convert_string_list_to_float_list(splitted_input);
	}
	

	return meassurements_list;
}

QList<QString> data_reader::split_line_into_values(QString& line, const char * delim) const
{
	QList<QString>splitted_input;
	std::ifstream line_stream(line.toStdString());
	std::string value;
	//if line is empty return
	if (!line_stream)return splitted_input;

	//while there is still input, split the line according to the delimiter
	while (std::getline(line_stream, value, *delim)) {
		QString temp = QString::fromStdString(value);
		splitted_input.push_back(temp);
	}

	return splitted_input;
}

QList<float> data_reader::convert_string_list_to_float_list(QList<QString>& string_value_list) const
{
	QList<float> float_value_list;
	//convert every string in the given list into a float value
	for (const QString& string_value : string_value_list) {
		float float_value;
		try {
			float float_value = string_value.toFloat();
			float_value_list.push_back(float_value);
		}
		catch (const std::invalid_argument& ia) {
			qDebug() << "Could not convert from string to float";
		}
	}
	return float_value_list;
}

QList<QPair<float, float>> data_reader::create_time_meassurement_pairs(QList<float>& meassurements, unsigned int frequency) const
{
	QList<QPair<float, float>> time_meassurement_pairs;
	float time = 0.0f;
	//for each meassurement value, create a time value
	for (const float& value : meassurements) {
		//time is increased each step with certain value(frequency) from 0 seconds on
		time += static_cast<float>(1.0f / frequency);
		QPair<float, float> time_value_pair(time, value);
		time_meassurement_pairs.push_back(time_value_pair);
	}
	return time_meassurement_pairs;
}

data_writer::data_writer()
{
}

data_writer::data_writer(QString & data_name):
	data_name_(data_name)
{
}

void data_writer::set_data_name(QString & data_name)
{
	data_name_ = data_name;
}

QString data_writer::get_data_name() const
{
	return data_name_;
}

bool data_writer::write_string_to_file(QString & line)
{
	if (data_name_.isEmpty()) {
		return false;
	}

	QFile q_file(data_name_);

	if (!q_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
		return false;
	}

	QTextStream outstream(&q_file);

	outstream << line;

	outstream << "\n";

	return true;
}

bool data_writer::write_list_to_file(QList<QString>& q_list)
{

	if (data_name_.isEmpty()) {
		return false;
	}

	QFile q_file(data_name_);

	if (!q_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
		return false;
	}

	QTextStream outstream(&q_file);

	for (QString& q_string : q_list) {
		outstream << q_string;
		outstream << " ";
	}
	
	outstream << " \n";

	return true;
}

bool data_writer::write_list_of_pairs_to_file(QList<QPair<QString, QString>>& pair_q_list)
{
	if (data_name_.isEmpty()) {
		return false;
	}

	QFile q_file(data_name_);

	if (!q_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
		return false;
	}

	QTextStream outstream(&q_file);

	for ( QPair<QString, QString>& q_pair : pair_q_list) {
		outstream << q_pair.first;
		outstream << " ";
		outstream << q_pair.second;
		outstream << " ";
	}

	outstream << "\n";

	return true;
}

bool data_writer::write_meassurements_as_csv(QString & channel, QList<float>& values)
{
	if (channel.isEmpty() || values.isEmpty()) {
		return false;
	}

	QString data_name = channel + ".csv";

	QFile q_file(data_name);

	if (!q_file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		return false;
	}

	QTextStream outstream(&q_file);

	for (float& value : values) {
		outstream << value;
		outstream << ",";
	}

	return true;
}

bool data_writer::write_meassurements_as_csv(QString & file_name, QString & channel, QList<float>& values)
{
	if (channel.isEmpty() || values.isEmpty() || file_name.isEmpty()) {
		return false;
	}

	QString data_name_temp = file_name;

	if (!file_name.endsWith(".csv")) {
		data_name_temp += ".csv";
	}

	QFile q_file(data_name_temp);

	if (!q_file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		return false;
	}

	QTextStream outstream(&q_file);

	outstream << channel;
	outstream << ",";

	int counter(1);

	for (float& value : values) {

		

		outstream << value;
		//just add a comma if it is not the last value
		if (counter != values.length()) {
			outstream << ",";
		}

		counter++;
	}

	return true;
}

bool data_writer::write_meassurements_as_csv(QString& data_name, QMap<QString, QList<float>>& meassurements)
{
	if (data_name.isEmpty() || meassurements.isEmpty()) {
		return false;
	}

	QString data_name_temp = data_name;

	if (!data_name.endsWith(".csv")) {
		data_name_temp += ".csv";
	}

	QFile q_file(data_name_temp);

	if (!q_file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		return false;
	}

	QTextStream outstream(&q_file);

	QMap<QString, QList<float>>::const_iterator map_iter (meassurements.constBegin()), map_end(meassurements.constEnd());
	//iterate over each channel in the QMap
	for (; map_iter != map_end; map_iter++) {
		
		const QList<float> channel_list = map_iter.value();
		QList<float>::const_iterator list_iter(channel_list.constBegin()), list_end(channel_list.constEnd());
		outstream << map_iter.key();
		//iterate over each of the channel's meassurements and save it file
		for (; list_iter != list_end; list_iter++) {
			
			float value (*list_iter);
			
			outstream << value;
			//just add a comma if it is the last value
			if (list_iter + 1 != list_end) {
				outstream << ",";
			}
		}

	}

	return true;
}


