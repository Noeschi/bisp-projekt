#pragma once

#include "vertex.h"
#include <QObject>
#include <GL\glew.h>
#include <GL\freeglut.h>
#include <QtWidgets\qwidget.h>
#include <QtWidgets\qopenglwidget.h>
#include <qopengl.h>
#include <qmap.h>
#include <qlist.h>


/**
* @brief The gl_widget class uses OpenGL to draw meassurements onto the screen. 
*/
class gl_widget: public QOpenGLWidget{
	Q_OBJECT
public:
	/**
	* @brief Constructor
	* @param parent QWidget parent of the gl_widget
	*/
	explicit gl_widget(QWidget *parent = 0);

	/**
	* @brief Deconstructor
	*/
	~gl_widget();
private slots:
	/**
	* @brief add_meassured_value is a slot function which receives a meassurement value and adds it to the vertices_ lsit to be drawn
	* @param value meassurment value to be added to vertices_ lsit
	*/
	void add_meassured_value(float value);
	
protected:
	
	/**
	* @brief initializeGL method is used to initiliaze mandatory OpenGL properties
	*/
	void initializeGL();

	/**
	* @brief paintGL method calls OpenGL methods and draw function
	*/
	void paintGL();

	/**
	* @brief resizeGL method is called when the window is resized the widget to a given size and height
	* @param width width of the resized widget
	* @param height height of the resized widget
	*/
	void resizeGL(int width, int height);

	/**
	* @brief draw method draws the coordinate system once and calls the draw_vertices method
	*/
	void draw();

	/**
	* @brief draw_coordinate_system draws the coordinate system to the screen
	*/
	void draw_coordinate_system();

	/**
	* @brief draw_verticees draws vertices in vertices_ to the screen
	*/
	void draw_vertices();

	/**
	* @brief draw_line draws a line between two ertices on the screen
	* @param first_vertex first vertex of the line
	* @param second_vertex second vertex of the line
	*/
	void draw_line(Vertex2D& first_vertex, Vertex2D& second_vertex);

	/**
	* @brief minimum_size_hint method returns the minimum size of the gl_widget
	* @return reasonable minimum size of the gl_widget
	*/
	QSize minimum_size_hint() const;

	/**
	* @brief size_hint returns a reasonable size for the gl_widget
	* @return seasonable size as QSize
	*/
	QSize size_hint() const;

	std::list<Vertex2D> vertices_;/**< list of vertices to be drawn*/

};

class evaluation_widget : public gl_widget {

public:
	/**
	* @brief Constructor
	* @param parent QWidget parent of the gl_widget
	*/
	explicit evaluation_widget(QMap<unsigned int, QList<float>>& channel_meassurements, float frequency, QWidget* parent = 0 );
	explicit evaluation_widget(QWidget* parent = 0);
	/**
	* @brief Deconstructor
	*/
	~evaluation_widget();

	void set_channel_meassurements(QMap<unsigned int, QList<float>>& channel_meassurements);

	void set_frequency(float frequency);

	void set_threshold_start_signal(float threshold);

	void set_threshold_shoot_signal(float threshold);

	float get_threshold_start_signal();

	float get_threshold_shoot_signal();


protected:


	void draw();

	/**
	* @brief paintGL method calls OpenGL methods and draw function
	*/
	void paintGL();


	/**
	* @brief draw_vertices draws vertices in channel_meassurements_ to the screen
	*/
	void draw_vertices();

	QVector3D get_color_for_channel(unsigned int channel_number);

	QMap<unsigned int, QList<Vertex2D>> channel_vertices_;


	void select_shooting_signals();

	void select_start_signal();

	void convert_meassurements_into_vertices(QMap<unsigned int, QList<float>>& channel_meassurements);

	float frequency_;

	QMap<unsigned int, QList<float>> channel_meassurements_;/**< Holds the meassurement values. Key is the channel number, value is the list of meassurements*/

	float max_pressure_;

	float threshold_start_signal_;

	float threshold_shoot_signal_;
};